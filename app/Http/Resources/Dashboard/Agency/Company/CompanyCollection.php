<?php

namespace App\Http\Resources\Dashboard\Agency\Company;

use App\Http\Resources\Dashboard\Agency\Employee\EmployeeResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'companies' => $this->collection->map(function ($company) use ($request) {
                return (new CompanyResource($company))->toArray($request);
            })
        ];
    }
}
