<?php

namespace App\Http\Resources\Dashboard\Agency\Company;

use App\Http\Resources\Dashboard\Agency\File\UserFilesCollection;
use App\Http\Resources\Dashboard\Agency\File\UserFilesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'website' => $this->website,
            'employees_number' => $this->employees()->count(),
            'date' => $this->created_at->format('Y-m-d'),
            'user_type_string' => __('Company'),
            'files' => $this->file == null  ? null : (new UserFilesResource($this->file))->toArray(),
            'company' => $this,
        ];
    }
}
