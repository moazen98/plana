<?php

namespace App\Http\Resources\Dashboard\Agency\Employee;

use App\Http\Resources\Dashboard\Agency\Company\CompanyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request=null)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->first_name.' '.$this->last_name,
            'full_name' => $this->name,
            'phone' => $this->phone,
            'international_code' => $this->international_code,
            'full_phone' => $this->full_phone,
            'email' => $this->email,
            'date' => $this->created_at->format('Y-m-d'),
            'company' => $this->company == null ? null : (new CompanyResource($this->company))->toArray(),
            'user_type_string' => __('Employee'),
        ];
    }
}
