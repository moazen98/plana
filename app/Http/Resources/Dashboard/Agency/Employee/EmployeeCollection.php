<?php

namespace App\Http\Resources\Dashboard\Agency\Employee;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'employees' => $this->collection->map(function ($employee) use ($request) {
                return (new EmployeeResource($employee))->toArray($request);
            })
        ];
    }
}
