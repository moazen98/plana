<?php

namespace App\Http\Resources\Dashboard\Role;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Session;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lang = mobile_user() == null ? app()->getLocale() : (Session::get('language') == null ? app()->getLocale() : Session::get('language'));

        return [
            'id' => $this->id,
            'name' => $lang == 'ar' ? $this['name_ar'] : $this['name'],
            'description' => $lang == 'ar' ? $this['description_ar'] : $this['description'],
            'employees_number' => $this->users()->count(),
            'basic_role' => $this->basic_role,
        ];
    }
}
