<?php

namespace App\Http\Controllers\Api;

use App\Enums\AgencyType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Authentication\ForgetPasswordStoreRequest;
use App\Http\Requests\Api\Authentication\RegularLoginCustomer;
use App\Http\Requests\Api\Authentication\RegularRegisterUser;
use App\Http\Requests\Api\Profile\UpdateProfilePasswordRequest;
use App\Http\Requests\Api\Profile\UpdateProfileRequest;
use App\Http\Resources\Dashboard\Agency\Branch\BranchCollection;
use App\Http\Resources\Dashboard\Agency\Dealer\DealerCollection;
use App\Http\Resources\Dashboard\Agency\Type\TypeCollection;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Responses\V1\CustomResponse;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends MainApiController
{


    public function login(RegularLoginCustomer $request)
    {

        if ($request->has('email')) {

            $user = app('servicesV1')->authenticationService->getUserMobileByEmail($request->email);

        } elseif ($request->has('phone')) {

            $user = app('servicesV1')->authenticationService->getUserMobileByPhone($request->phone);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.server_error'), $data = [], []);
        }

        if (!is_null($user)) {

            $type = getUserAgencyType($user);
            $checkUserVerified = CheckUserVerified($type, $user);

            if (!$checkUserVerified) {
                return CustomResponse::Failure(Response::HTTP_FORBIDDEN, trans('mobile_message.account_not_verified'), $data = [], []);
            }

            $checkUserStatus = CheckUserStatus($type, $user);

            if (!$checkUserStatus) {
                return CustomResponse::Failure(Response::HTTP_FORBIDDEN, trans('mobile_message.account_not_activate'), $data = [], []);
            }

            if (Hash::check($request->password, $user->authentication->password)) {

                if (is_null($request->fcm_token)) {
                    return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.fcm_token_not_found'), $data = [], []);
                }

                $token = app('servicesV1')->authenticationService->userAuthenticationGenerate($user, $request->fcm_token);

                app('servicesV1')->notificationService->registerToken($request->fcm_token, "users_all");


                $title = __('A new user login');
                $body = $user->authentication == null ? __('Unknown user') : $user->authentication->email . __('has been login to the system');

                app('servicesV1')->notificationService->sendUserToTopic($title, $body);

                $userResource = getUserAgencyResource($type, $user);

                return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.login_success'), $data = ['user' => $userResource, 'token' => $token, 'token_type' => 'Bearer'], []);

            } else {
                return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.username_password_wrong'), [], []);
            }
        }
    }


    public function register(RegularRegisterUser $request)
    {

        $user = app('servicesV1')->authenticationService->storeUser($request);

        if (!$user) {
            return CustomResponse::Failure(Response::HTTP_FORBIDDEN, trans('mobile_message.failed_create_account'), $data = [], []);
        }


        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.success_create_account'), $data = [], []);

    }

    public function forgetPassword(ForgetPasswordStoreRequest $request){

        $title = __('A new forget password order');
        $body =  $request->credential .' '. __('has been send new forget password order');

        app('servicesV1')->notificationService->sendUserToTopic($title, $body);

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.order_success_send'), $data = [], []);
    }

    public function userProfile(Request $request){

        $user = getUserMobile($request);

        $data = getUserAgencyResource(get_class($user),$user);

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function userProfileImage(UpdateProfileRequest $request){


        $result = app('servicesV1')->authenticationService->updateProfile($request);

        if ($result){
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);
        }else{
            return CustomResponse::Failure(Response::HTTP_INTERNAL_SERVER_ERROR, trans('mobile_message.failed'), [], []);
        }
    }


    public function userProfilePassword(UpdateProfilePasswordRequest $request){

        $result = app('servicesV1')->authenticationService->updateProfilePassowrd($request);

        if ($result['status']){
            return CustomResponse::Success(Response::HTTP_OK, $result['message'], [], []);
        }else{
            return CustomResponse::Failure(Response::HTTP_UNPROCESSABLE_ENTITY, $result['message'], [], []);
        }

    }


    public function getAgencyValue(){

        $data = app('servicesV1')->authenticationService->getAgencyValue();

        $data = (new TypeCollection($data))->toArray();
        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function logout(Request $request)
    {

        $status = app('servicesV1')->authenticationService->logoutApi($request);

        if ($status){
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.logout_success'), [], []);
        }else{
            return CustomResponse::Failure(Response::HTTP_UNPROCESSABLE_ENTITY, trans('mobile_message.failed'), [], []);
        }
    }


    public function getDealersBranches(){

        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $data = (new UserCollection($usersBranchDealer))->mainDetailsUser();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }


    public function getAllUsersSearch(Request $request){

        $users = app('servicesV1')->authenticationService->searchAllUsersApi($request);

        $data = (new UserCollection($users))->mainDetailsUser();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function getAllBranches(){

        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchUsers()->paginate($this->apiPaginate);
        $data = (new BranchCollection($usersBranchDealer))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);


    }

    public function getAllDealers(){

        $usersBranchDealer = app('servicesV1')->authenticationService->getDealersUsers()->paginate($this->apiPaginate);
        $data = (new DealerCollection($usersBranchDealer))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);


    }


    public function getAllUsers(){

        $usersBranchDealer = app('servicesV1')->authenticationService->getAllTypeUsers();
        $data = (new UserCollection($usersBranchDealer))->mainDetailsUser();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }
}
