<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Setting\Hide\HideStoreRequest;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MainSettingController extends Controller
{


    public function hideItem(HideStoreRequest $request){


        $agency = getUserMobile($request);

        if (!$agency){
            return CustomResponse::Failure(Response::HTTP_NOT_FOUND, trans('mobile_message.customer_not_found'), $data = [], []);
        }

        $authentication = $agency->authentication;

        if (!$authentication){
            return CustomResponse::Failure(Response::HTTP_NOT_FOUND, trans('mobile_message.customer_not_found'), $data = [], []);
        }

        $item = getItemType($request->item_type , $request->item_id);

        if (!$item){
            return CustomResponse::Failure(Response::HTTP_NOT_FOUND, trans('mobile_message.item_not_found'), $data = [], []);
        }

        $result = app('servicesV1')->commonSettingService->hideItem($authentication,$item,$request->item_type);


        if ($result){
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.hide_success'), [], []);
        }else{
            return CustomResponse::Failure(Response::HTTP_INTERNAL_SERVER_ERROR, trans('mobile_message.failed'), [], []);
        }
    }
}
