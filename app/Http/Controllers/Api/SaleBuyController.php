<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Operation\OperationStoreRequest;
use App\Http\Resources\Dashboard\Operation\OperationCollection;
use App\Http\Resources\Dashboard\Operation\OperationResource;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SaleBuyController extends MainApiController
{


    public function storeSaleBuy(OperationStoreRequest $request)
    {

        $result = app('servicesV1')->saleBuyService->storeOperation($request);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.success'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }

    public function getSaleBuy(Request $request)
    {

        $result = app('servicesV1')->saleBuyService->getSale($request->id);

        $data = (new OperationResource($result))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }


    public function updateSaleBuyStatus(Request $request)
    {
        $result = app('servicesV1')->saleBuyService->updateSaleStatus($request, $request->id, $request->type);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

    }

    public function updateSaleBuy(OperationStoreRequest $request)
    {

        $result = app('servicesV1')->saleBuyService->updateOperation($request, $request->id);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }


    public function getSaleBuyStatus(Request $request)
    {

        $result = app('servicesV1')->saleBuyService->getSaleBuyStatus();

        $data = (new OperationCollection($result))->withTypes();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function getAllSaleBuy(Request $request)
    {

        $result = app('servicesV1')->saleBuyService->apiOperationFilter($request)->paginate($this->apiPaginate);

        $data = (new OperationCollection($result))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);


    }


    public function SaleBuyDelete(Request $request)
    {

        $result = app('servicesV1')->saleBuyService->destroyOperation($request->id);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_INTERNAL_SERVER_ERROR, trans('mobile_message.failed'), [], []);
        }

    }
}
