<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LocationController extends MainApiController
{


    public function getCountry(Request $request){

        $countries = app('servicesV1')->locationService->getAllCountries($request);
        $data = (new CountryCollection($countries))->toArray($request);

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }


    public function getCityByCountry(Request $request){


        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($request->id);
        $data = (new CityCollection($citiesData))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);
    }
}
