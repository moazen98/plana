<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CurrenciesController extends Controller
{


    public function getCurrencies(){

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $data = (new CurrencyCollection($currenciesData))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }
}
