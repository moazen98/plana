<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Transfer\Outgoing\TransferOutgoingStoreRequest;
use App\Http\Requests\Api\Transfer\TransferChangeStatusRequest;
use App\Http\Requests\Api\Transfer\TransferIncomingStoreRequest;
use App\Http\Resources\Dashboard\Transfer\Incoming\TransferIncomingCollection;
use App\Http\Resources\Dashboard\Transfer\Incoming\TransferIncomingResource;
use App\Http\Resources\Dashboard\Transfer\Outgoing\TransferOutgoingCollection;
use App\Http\Resources\Dashboard\Transfer\Outgoing\TransferOutgoingResource;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TransferOutgoingController extends MainApiController
{


    public function getAllTransfer(Request $request)
    {

        if ($request->type == 0) {
            $transfers = app('servicesV1')->transferOutgoingService->filterTransfersApi($request)->paginate($this->apiPaginate);
            $data = (new TransferOutgoingCollection($transfers))->toArray($request);

        } elseif ($request->type == 1) {

            $transfers = app('servicesV1')->transferIncomingService->filterTransfersApi($request)->paginate($this->apiPaginate);
            $data = (new TransferIncomingCollection($transfers))->toArray($request);

        }else{
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function getOutgoingTransfer(Request $request){

        $transfer = app('servicesV1')->transferOutgoingService->getTransferById($request->id);
        $data = (new TransferOutgoingResource($transfer))->toArray($request);
        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }
    public function storeOutgoingTransfer(TransferOutgoingStoreRequest $request){

        $agencyId = $request->transfarable_id;
        $agencyType = $request->transfarable_type;
        $result = app('servicesV1')->transferOutgoingService->storeTransfer($request,$agencyId,$agencyType);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.success'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

    }


    public function updateOutgoingTransfer(TransferOutgoingStoreRequest $request){

        $agencyId = $request->transfarable_id;
        $agencyType = $request->transfarable_type;
        $result = app('servicesV1')->transferOutgoingService->updateTransfer($request,$request->id,$agencyId,$agencyType);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

    }

    public function changeTransferStatus(TransferChangeStatusRequest $request){

        if ($request->type == 0) {
            $transfers = app('servicesV1')->transferOutgoingService->getTransferById($request->id);

        } elseif ($request->type == 1) {

            $transfers = app('servicesV1')->transferIncomingService->getTransferById($request->id);

        }else{
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }


        $result = app('servicesV1')->transferOutgoingService->changeTransferStatus($transfers,$request);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

    }

    public function deleteOutgoingTransfer(Request $request){

        $result = app('servicesV1')->transferOutgoingService->destroyTransfer($request->id);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }
}
