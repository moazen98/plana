<?php

namespace App\Http\Controllers\Api;

use App\Enums\CreditStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Credit\CreditStoreRequest;
use App\Http\Requests\Api\Credit\CreditUpdateRequest;
use App\Http\Resources\Dashboard\Credit\CreditCollection;
use App\Http\Resources\Dashboard\Credit\CreditResource;
use App\Http\Resources\Dashboard\Status\StatusCollection;
use App\Http\Resources\Dashboard\Status\StatusResource;
use App\Http\Resources\Dashboard\Transfer\Incoming\TransferIncomingCollection;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CreditController extends MainApiController
{


    public function store(CreditStoreRequest $request)
    {

        $sourceID = $request->authentication_source_id;
        $sourceType = $request->authentication_source_type;

        $toID = $request->authentication_to_id;
        $toType = $request->authentication_to_type;

        $result = app('servicesV1')->creditService->storeCredit($request, $sourceID, $sourceType, $toID, $toType);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.success'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }


    public function update(CreditUpdateRequest $request)
    {

        $sourceID = $request->authentication_source_id;
        $sourceType = $request->authentication_source_type;

        $toID = $request->authentication_to_id;
        $toType = $request->authentication_to_type;

        $result = app('servicesV1')->creditService->updateCredit($request, $request->id, $sourceID, $sourceType, $toID, $toType);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }

    public function index(Request $request)
    {

        $credits = app('servicesV1')->creditService->creditsSearchApi($request)->paginate($this->apiPaginate);

        $data = (new CreditCollection($credits))->toArray($request);

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }


    public function show(Request $request)
    {

        $credits = app('servicesV1')->creditService->getCreditById($request->id);

        $data = (new CreditResource($credits))->toArray($request);

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function destroy(Request $request)
    {

        $result = app('servicesV1')->creditService->destroyCredit($request->id);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }

    public function changeCreditStatus(Request $request)
    {

        $result = app('servicesV1')->creditService->changeCreditStatus($request->id);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), [], []);
        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }

    public function getCreditStatus(){

        $result = app('servicesV1')->creditService->getCreditStatus();

        $data = (new StatusCollection($result))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), $data, []);

    }
}
