<?php

namespace App\Http\Controllers\Api;

use App\Enums\UsageInformationType;
use App\Http\Controllers\Controller;
use App\Http\Resources\Dashboard\UsageInformation\UsageResource;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ContentController extends MainApiController
{



    public function getAboutUs(){

        $about = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::ABOUT_US);

        $data = (new UsageResource($about))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);
    }



    public function getPrivacy(){

        $privacy = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::PRIVACY);

        $data = (new UsageResource($privacy))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);
    }


    public function getPayment(){

        $payment = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::PAYMENT);

        $data = (new UsageResource($payment))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);
    }


    public function getTerm(){

        $term = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::TERMS);

        $data = (new UsageResource($term))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);
    }


}
