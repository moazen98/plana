<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Offer\CompleteInformationStoreRequest;
use App\Http\Requests\Api\Offer\OfferStoreRequest;
use App\Http\Requests\Api\Offer\OfferUpdateRequest;
use App\Http\Requests\Api\Operation\OperationStoreRequest;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Resources\Dashboard\Offer\OfferCollection;
use App\Http\Resources\Dashboard\Offer\OfferResource;
use App\Http\Responses\V1\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OfferController extends MainApiController
{

    public function storeOffer(OfferStoreRequest $request)
    {


        $result = app('servicesV1')->offerOrderService->storeOffer($request);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.success'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }


    public function getOffer(Request $request){

        $result = app('servicesV1')->offerOrderService->getOffer($request->id);

        if ($result) {
            $data =  (new OfferResource($result))->toArray();
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }


    public function updateOrderOffer(OfferUpdateRequest $request)
    {

        $result = app('servicesV1')->offerOrderService->updateOffer($request,$request->id);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }

    public function getOfferOrderStatus(Request $request){

        $result = app('servicesV1')->offerOrderService->getOfferOrderStatus();

        $data = (new OfferCollection($result))->withTypes();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function getAllOrderOffer(Request $request){

        $result = app('servicesV1')->offerOrderService->apiOfferFilter($request)->paginate($this->apiPaginate);

        $data = (new OfferCollection($result))->toArray();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);


    }


    public function getAllTypeUsers(Request $request){

        $result = app('servicesV1')->authenticationService->getAllTypeUsers();

        $data = (new UserCollection($result))->mainDetailsUser();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);

    }

    public function completeInformation(CompleteInformationStoreRequest $request)
    {

        $offer = app('servicesV1')->offerOrderService->getOffer($request->offer_id);

        $result = app('servicesV1')->offerOrderService->completeInformationOffer($offer, $request);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.complete_success'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }
    }


    public function orderOfferDelete(Request $request)
    {

        $result = app('servicesV1')->offerOrderService->destroyOffer($request->id);

        if ($result) {
            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.delete'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_INTERNAL_SERVER_ERROR, trans('mobile_message.failed'), [], []);
        }
    }


    public function getCommissionType(){

        $result = app('servicesV1')->offerOrderService->getOfferOrderCommissionType();

        $data = (new OfferCollection($result))->withTypes();

        return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.upload_successful'), $data, []);


    }


    public function updateOrderOfferStatus(Request $request)
    {
        $result = app('servicesV1')->offerOrderService->updateOfferStatus($request, $request->id, $request->type);

        if ($result) {

            return CustomResponse::Success(Response::HTTP_OK, trans('mobile_message.update'), [], []);

        } else {
            return CustomResponse::Failure(Response::HTTP_EXPECTATION_FAILED, trans('mobile_message.failed'), [], []);
        }

    }
}
