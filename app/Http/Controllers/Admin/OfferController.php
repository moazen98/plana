<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OfferExport;
use App\Exports\SaleExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Offer\CompleteInformationStoreRequest;
use App\Http\Requests\Admin\Offer\OfferStoreRequest;
use App\Http\Requests\Admin\Offer\OfferUpdateRequest;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Resources\Dashboard\Agency\User\UserResource;
use App\Http\Resources\Dashboard\Commission\CommissionCollection;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Offer\OfferResource;
use App\Http\Resources\Dashboard\Status\StatusCollection;
use App\Models\Commission;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OfferController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_offer'])->only('index');
        $this->middleware(['permission:create_offer'])->only('create');
        $this->middleware(['permission:update_offer'])->only('edit');
        $this->middleware(['permission:delete_offer'])->only('destroy');
        $this->middleware(['permission:read_offer'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Order/Offer')]
        ];

        $offers = app('servicesV1')->offerOrderService->getAllOffer()->paginate($this->dashboardPaginate);


        $offers->getCollection()->transform(function ($item) use ($request) {
            return (new OfferResource($item))->toArray($request);
        });

        $statusData = app('servicesV1')->offerOrderService->getStatus();
        $status = (new StatusCollection($statusData))->toArray($request);

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray($request);

        return view('admin.offer.index', compact('offers', 'status', 'currencies', 'breadcrumbs'));
    }


    public function fetch_data(Request $request)
    {

        $offers = app('servicesV1')->offerOrderService->filterOrders($request)->paginate($this->dashboardPaginate);

        $offers->getCollection()->transform(function ($item) use ($request) {
            return (new OfferResource($item))->toArray($request);
        });

        return view('admin.offer.include.pagination_data', compact('offers'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('offer.index'), 'name' => __('Order/Offer')],
            ['name' => __('Create')]
        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $userData = getCurrentDashboardUser();
        $user = (new UserResource($userData))->toArray();
        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();
        $commissionsData = Commission::all();
        $commissions = (new CommissionCollection($commissionsData))->toArray();


        return view('admin.offer.create', compact('currencies', 'user', 'countries', 'commissions', 'breadcrumbs'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferStoreRequest $request)
    {
        $result = app('servicesV1')->offerOrderService->storeOffer($request);

        if ($result) {
            return redirect()->route('offer.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('offer.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = app('servicesV1')->offerOrderService->getOffer($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('offer.index'), 'name' => __('Order/Offer')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $offer = (new OfferResource($result))->toArray();
        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $userData = getCurrentDashboardUser();
        $user = (new UserResource($userData))->toArray();
        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();
        $commissionsData = Commission::all();
        $commissions = (new CommissionCollection($commissionsData))->toArray();

        return view('admin.offer.show', compact('offer', 'currencies', 'user', 'countries', 'commissions', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = app('servicesV1')->offerOrderService->getOffer($id);


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('offer.index'), 'name' => __('Order/Offer')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $offer = (new OfferResource($result))->toArray();
        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $userData = getCurrentDashboardUser();
        $user = (new UserResource($userData))->toArray();
        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($result->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $commissionsData = Commission::all();
        $commissions = (new CommissionCollection($commissionsData))->toArray();

        return view('admin.offer.edit', compact('cities', 'offer', 'currencies', 'user', 'countries', 'commissions', 'breadcrumbs'));

    }

    public function completeInformation($id)
    {
        $result = app('servicesV1')->offerOrderService->getOffer($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('offer.index'), 'name' => __('Order/Offer')],
            ['name' => __('Complete information')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $offer = (new OfferResource($result))->toArray();
        $userData = app('servicesV1')->authenticationService->getAllTypeUsers();
        $users = (new UserCollection($userData))->mainDetailsUser();
        $commissionsData = Commission::all();
        $commissions = (new CommissionCollection($commissionsData))->toArray();
        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $userData = getCurrentDashboardUser();
        $user = (new UserResource($userData))->toArray();

        return view('admin.offer.complete', compact('user', 'currencies', 'commissions', 'users', 'offer', 'breadcrumbs'));
    }


    public function completeInformationPost(CompleteInformationStoreRequest $request, $id)
    {

        $offer = app('servicesV1')->offerOrderService->getOffer($id);

        app('servicesV1')->offerOrderService->completeInformationOffer($offer,$request);

        return redirect()->route('offer.index')->with('success', __('message.complete_success'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OfferUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->offerOrderService->updateOffer($request, $id);

        if ($result) {
            return redirect()->route('offer.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('offer.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->offerOrderService->destroyOffer($id);

        if ($result) {
            return redirect()->route('offer.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('offer.index')->with('failed', __('message.failed'));
        }
    }

    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new OfferExport(), __('Orders-Offers') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('offer.index')->with('failed', __('message.failed'));
        }
    }
}
