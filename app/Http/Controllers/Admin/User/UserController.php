<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\UserExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\User\UserStoreRequest;
use App\Http\Requests\Admin\User\UserUpdateRequest;
use App\Http\Resources\Dashboard\Agency\User\UserResource;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Models\Role;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_user'])->only('index');
        $this->middleware(['permission:create_user'])->only('create');
        $this->middleware(['permission:update_user'])->only('edit');
        $this->middleware(['permission:delete_user'])->only('destroy');
        $this->middleware(['permission:read_user'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Users')]
        ];

        $users = app('servicesV1')->userService->getAllUsers()->paginate($this->dashboardPaginate);

        $users->getCollection()->transform(function ($item) use ($request) {
            return (new UserResource($item))->toArray($request);
        });

        $rolesCollection = app('servicesV1')->userService->getAllRoles()->get();

        $roles = (new RoleCollection($rolesCollection))->toArray();

        return view('admin.user.page.index', compact('roles','users', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {


            $users = app('servicesV1')->userService->filterUsers($request)->paginate($this->dashboardPaginate);

            $users->getCollection()->transform(function ($item) use ($request) {
                return (new UserResource($item))->toArray($request);
            });

            return view('admin.user.page.include.pagination_data', compact('users'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('user.index'), 'name' => __('Users')],
            ['name' => __('Add')]
        ];

        $rolesCollection = Role::all();
        $roles = (new RoleCollection($rolesCollection))->toArray(null);

        return view('admin.user.page.create', compact('roles', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {

        $employees = app('servicesV1')->userService->storeUser($request);

        if ($employees) {
            return redirect()->route('user.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('user.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = app('servicesV1')->userService->getUserById($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/employee/index", 'name' => __('Employees')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . ' - ' . $result->first_name . ' ' . $result->last_name . ' - ' . $result->created_at->format('m/d/Y')]

        ];

        $user = (new UserResource($result))->toArray();

        $role = $result->roles()->first();


        return view('admin.user.page.show', compact('user', 'breadcrumbs', 'role'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $result = app('servicesV1')->userService->getUserById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('user.index'), 'name' => __('Employees')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . ' - ' . $result->first_name . ' ' . $result->last_name . ' - ' . $result->created_at->format('m/d/Y')]

        ];


        $user = (new UserResource($result))->toArray();
        $rolesCollection = Role::all();
        $roles = (new RoleCollection($rolesCollection))->toArray();


        return view('admin.user.page.edit', compact('user', 'roles', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->userService->updateUser($request, $id);

        if ($result) {
            return redirect()->route('user.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('user.index')->with('failed', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->userService->destroyUserById($id);

        if ($result) {
            return redirect()->route('user.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('user.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        ob_end_clean();

        return Excel::download(new UserExport(), __('Users') . '.xlsx');
    }
}
