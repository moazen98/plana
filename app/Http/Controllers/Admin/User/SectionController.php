<?php

namespace App\Http\Controllers\Admin\User;

use App\Exports\SectionExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\Section\SectionStoreRequest;
use App\Http\Requests\Admin\User\Section\SectionUpdateRequest;
use App\Models\Section\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class SectionController extends MainDashboardController
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_section'])->only('index');
        $this->middleware(['permission:create_section'])->only('create');
        $this->middleware(['permission:update_section'])->only('edit');
        $this->middleware(['permission:delete_section'])->only('destroy');
        $this->middleware(['permission:read_section'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['name' => __('Sections')]
        ];

        $sections = Section::when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('name', '%' . $request->search . '%')
                ->orWhere('description', '%' . $request->search . '%');

        })->latest()->paginate($this->dashboardPaginate);

        return view('admin.user.section.index', compact('sections', 'breadcrumbs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Add')]
        ];

        return view('admin.user.section.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SectionStoreRequest $request)
    {
        DB::beginTransaction();
        $section = new Section();

        $section->setImage_urlAttributes($request);
        $section->setTranslatedAttributes($request);
        $section->active = $request->has('active') ? 1 : 0;
        $section->description = $request->description;
        $section->save();

        DB::commit();

        return redirect()->route('section.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $section = Section::findOrFail($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Show')],
            ['name' => "#" . $section->id . ' - ' . $section->name . ' - ' . $section->created_at->format('m/d/Y')]

        ];


        $employees = $section->employees;


        return view('admin.user.section.show', compact('section', 'employees', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::findOrFail($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => "/admin/users/section/index", 'name' => __('Sections')],
            ['name' => __('Edit')],
            ['name' => "#" . $section->id . ' - ' . $section->name . ' - ' . $section->created_at->format('m/d/Y')]

        ];




        return view('admin.user.section.edit', compact('section', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SectionUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        $section = Section::findOrFail($id);

        if ($request->has('image')) {
            $section->setImage_urlAttributes($request);
        }
        $section->setTranslatedAttributes($request);
        $section->active = $request->has('active') ? 1 : 0;
        $section->description = $request->description;
        $section->save();
        DB::commit();

        return redirect()->route('section.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $section = Section::findOrFail($id);
        $section->delete();
        DB::commit();

        return redirect()->route('section.index')->with('success', __('message.delete'));
    }

    public function export()
    {
        return Excel::download(new SectionExport(), 'sections.xlsx');
    }
}
