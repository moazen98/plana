<?php

namespace App\Http\Controllers\Admin;

use App\Enums\NotificationSendType;
use App\Enums\NotificationType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Notification\NotificationStoreRequest;
use App\Http\Requests\Admin\Notification\NotificationUpdateRequest;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Customer;
use App\Models\CustomerNotification;
use App\Models\FcmToken;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Database;

class NotificationController extends MainDashboardController
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_notification'])->only('index');
        $this->middleware(['permission:create_notification'])->only('create');
        $this->middleware(['permission:update_notification'])->only('edit');
        $this->middleware(['permission:delete_notification'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['name' => __('Notifications')],
        ];


        $notifications = Notification::when($request->search, function ($q) use ($request) {
            return $q->where('title_ar', 'like', '%' . $request->search . '%')
                ->orWhere('title_en', 'like', '%' . $request->search . '%');
        })->latest()->paginate(10);


        return view('admin.notification.index', compact('notifications', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/notification/index", 'name' => __('Notifications')],
            ['name' => __('Create')],
        ];

        $countries = Country::has('cities')->get();
        $customers = Customer::all()->where('active', '=', 1);

        return view('admin.notification.create', compact('countries', 'customers', 'breadcrumbs'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationStoreRequest $request)
    {

        $notification = new Notification();
        $notification->title_ar = $request->title_ar;
        $notification->title_en = $request->title_en;
        $notification->description_ar = $request->description_ar;
        $notification->description_en = $request->description_en;
        $notification->type = $request->type;
        if ($request->has('country')) {
//            $country = Country::findOrFail($request->country);
//            $notification->country()->associate($country);
        }

        if ($request->has('city')) {
//            $city = City::findOrFail($request->city);
//            $notification->city()->associate($city);
        }

        $notification->notification_send_type = $request->has('check_customers') ? NotificationSendType::ALL : NotificationSendType::PART;

        $notification->save();


        if (!$request->has('check_customers')) {

            if ($request->has('customers')) {

                foreach ($request->customers as $key => $value) {
                    $notification->customers()->attach($value, ['is_read' => 0, 'date_read' => null, 'created_at' => now()]);
                }

                //TODO: here to send notification for all customers Topic
//                app('servicesV1')->notificationService->sendNotificationForUser($request->title_ar, $notification->description_ar, 'users_all'); //TODO:you should send ar and en
            }
        } else {

            $customers = Customer::where('active', 1)->get();
            foreach ($customers as $key => $customer) {
                $notification->customers()->attach($customer, ['is_read' => 0, 'date_read' => null, 'created_at' => now()]);
                //TODO: here to send notification for specifics customers
//                app('servicesV1')->notificationService->registerToken($customer->id, $request->title_ar, $notification->description_ar); //TODO:you should send ar and en
            }
        }



        return redirect()->route('notification.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/notification/index", 'name' => __('Notifications')],
            ['name' => __('Show')],
        ];

        $notification = Notification::find($id);

        return view('admin.notification.show', compact('notification', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/notification/index", 'name' => __('Notifications')],
            ['name' => __('Edit')],
        ];

        $notification = Notification::find($id);

        $countries = Country::has('cities')->get();

        $cities = array();

        if (!is_null($notification->country)) {
            $country_id = $notification->country;
            $cities = City::where('country_id', '=', $country_id->id)->get();
        }

        $customers = Customer::all();


        return view('admin.notification.edit', compact('notification', 'countries', 'customers', 'cities', 'breadcrumbs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationUpdateRequest $request, $id)
    {
        $notification = Notification::find($id);
        $notification->title_ar = $request->title_ar;
        $notification->title_en = $request->title_en;
        $notification->description_ar = $request->description_ar;
        $notification->description_en = $request->description_en;
        $notification->type = $request->type;
        if ($request->has('country')) {
            $notification->country()->associate($request->country);
        }
        if ($request->has('city')) {
            $notification->city()->associate($request->city);
        }
        if ($request->has('category_id')) {
            $notification->category()->associate($request->category_id);
        }

        $notification->save();

        if ($request->has('invoice_customer')) {

            $customerNotifications = CustomerNotification::where('notification_id', '=', $notification->id)->get();

            foreach ($customerNotifications as $noty) {
                $noty->delete();
            }

            foreach ($request->invoice_customer as $key => $value) {
                $notification->customers()->attach($value);
            }
        }

        return redirect()->route('notification.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::find($id);
        $customerNotifications = CustomerNotification::where('notification_id', '=', $notification->id)->get();

        foreach ($customerNotifications as $noty) {
            $noty->delete();
        }
        $notification->delete();

        return redirect()->route('notification.index')->with('success', __('message.delete'));

    }

    public function updateFcmTokenAdmin(Request $request)
    {

        app('servicesV1')->authenticationService->updateFcmTokenUser($request);

        return true;
    }


    public function updateFcmTokenCustomer(Request $request)
    {
        $customer = Auth::guard('customers')->user();
        if (!is_null($customer)) {
            $fcm = $customer->fcms()->where('fcm', $request->notification_token)->first();
            if (!is_null($fcm)) {
                $fcm->update([
                    'fcm' => $request->notification_token,
                    'register_date' => now(),
                    'register_expire' => null,
                ]);
            } else {
                $customer->fcms()->updateOrCreate([
                    'fcm' => $request->notification_token,
                    'register_date' => now(),
                    'register_expire' => null,
                ]);
            }

            app('servicesV1')->notificationService->registerToken($request->notification_token, "customers_all");
        } else {

            $fcm = FcmToken::firstOrCreate([
                'fcm' => $request->notification_token
            ], [
                'fcm' => $request->notification_token,
                'register_date' => now(),
                'register_expire' => null
            ]);

            app('servicesV1')->notificationService->registerToken($request->notification_token, "customers_all");
        }
        return true;
    }
}
