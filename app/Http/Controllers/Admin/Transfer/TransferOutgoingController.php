<?php

namespace App\Http\Controllers\Admin\Transfer;

use App\Exports\OutgoingTransferExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\Transfer\Outgoing\TransferOutgoingStoreRequest;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Transfer\Outgoing\TransferOutgoingResource;
use App\Http\Resources\Dashboard\Transfer\TransferTypeCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TransferOutgoingController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_transfer_outgoing'])->only('index');
        $this->middleware(['permission:create_transfer_outgoing'])->only('create');
        $this->middleware(['permission:update_transfer_outgoing'])->only('edit');
        $this->middleware(['permission:delete_transfer_outgoing'])->only('destroy');
        $this->middleware(['permission:read_transfer_outgoing'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Outgoing transfer')]
        ];

        $transfers = app('servicesV1')->transferOutgoingService->getAllTransfers()->paginate($this->dashboardPaginate);


        $transfers->getCollection()->transform(function ($item) use ($request) {
            return (new TransferOutgoingResource($item))->toArray($request);
        });


        $statusData = app('servicesV1')->transferOutgoingService->transferStatus();
        $status = (new TransferTypeCollection($statusData))->toArray($request);

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray($request);

        $usersData = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $users = (new UserCollection($usersData))->mainDetailsUser();


        return view('admin.transfer.transfer_outgoing.index', compact('transfers', 'status','currencies','users', 'breadcrumbs'));
    }

    public function fetch_data(Request $request)
    {

//        dump($request->all());die();

        $transfers = app('servicesV1')->transferOutgoingService->filterTransfers($request)->paginate($this->dashboardPaginate);

        $transfers->getCollection()->transform(function ($item) use ($request) {
            return (new TransferOutgoingResource($item))->toArray($request);
        });

        return view('admin.transfer.transfer_outgoing.include.pagination_data', compact('transfers'))->render();

    }


    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.outgoing.index'), 'name' => __('Outgoing transfer')],
            ['name' => __('Create')]
        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();

        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();


        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $usersSpecific = (new UserCollection($usersBranchDealer))->mainDetailsUser();


        return view('admin.transfer.transfer_outgoing.create', compact('currencies', 'countries', 'usersSpecific', 'breadcrumbs'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransferOutgoingStoreRequest $request)
    {

        $result = app('servicesV1')->transferOutgoingService->storeTransfer($request);

        if ($result) {
            return redirect()->route('transfer.outgoing.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('transfer.outgoing.index')->with('failed', __('message.failed'));
        }
    }


    public function edit($id)
    {
        $result = app('servicesV1')->transferOutgoingService->getTransferById($id);


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.outgoing.index'), 'name' => __('Outgoing transfer')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];


        $transfer = (new TransferOutgoingResource($result))->toArray();
        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($result->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $usersSpecific = (new UserCollection($usersBranchDealer))->mainDetailsUser();
        return view('admin.transfer.transfer_outgoing.edit', compact('cities','usersSpecific', 'transfer', 'currencies', 'countries', 'breadcrumbs'));

    }


    public function show($id)
    {
        $result = app('servicesV1')->transferOutgoingService->getTransferById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.outgoing.index'), 'name' => __('Outgoing transfer')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]
        ];

        $transfer = (new TransferOutgoingResource($result))->toArray();


        return view('admin.transfer.transfer_outgoing.show', compact('transfer','breadcrumbs'));

    }

    public function update(TransferOutgoingStoreRequest $request, $id){

        $result = app('servicesV1')->transferOutgoingService->updateTransfer($request,$id);

        if ($result) {
            return redirect()->route('transfer.outgoing.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('transfer.outgoing.index')->with('failed', __('message.failed'));
        }
    }


    public function destroy($id)
    {

        $result = app('servicesV1')->transferOutgoingService->destroyTransfer($id);

        if ($result) {
            return redirect()->route('transfer.outgoing.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('transfer.outgoing.index')->with('failed', __('message.failed'));
        }
    }

    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new OutgoingTransferExport(), __('Outgoing-Transfers') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('transfer.outgoing.index')->with('failed', __('message.failed'));
        }
    }
}
