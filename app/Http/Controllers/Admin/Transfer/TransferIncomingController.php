<?php

namespace App\Http\Controllers\Admin\Transfer;

use App\Exports\IncomingTransferExport;
use App\Exports\OutgoingTransferExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Transfer\Incoming\TransferIncomingStoreRequest;
use App\Http\Requests\Admin\Transfer\Outgoing\TransferOutgoingStoreRequest;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Transfer\Incoming\TransferIncomingResource;
use App\Http\Resources\Dashboard\Transfer\Outgoing\TransferOutgoingResource;
use App\Http\Resources\Dashboard\Transfer\TransferTypeCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TransferIncomingController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_transfer_incoming'])->only('index');
        $this->middleware(['permission:create_transfer_incoming'])->only('create');
        $this->middleware(['permission:update_transfer_incoming'])->only('edit');
        $this->middleware(['permission:delete_transfer_incoming'])->only('destroy');
        $this->middleware(['permission:read_transfer_incoming'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Incoming transfer')]
        ];

        $transfers = app('servicesV1')->transferIncomingService->getAllTransfers()->paginate($this->dashboardPaginate);


        $transfers->getCollection()->transform(function ($item) use ($request) {
            return (new TransferIncomingResource($item))->toArray($request);
        });


        $statusData = app('servicesV1')->transferIncomingService->transferStatus();
        $status = (new TransferTypeCollection($statusData))->toArray($request);

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray($request);

        $usersData = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $users = (new UserCollection($usersData))->mainDetailsUser();

        return view('admin.transfer.transfer_incoming.index', compact('transfers', 'status','currencies','users', 'breadcrumbs'));
    }

    public function fetch_data(Request $request)
    {

        $transfers = app('servicesV1')->transferIncomingService->filterTransfers($request)->paginate($this->dashboardPaginate);

        $transfers->getCollection()->transform(function ($item) use ($request) {
            return (new TransferIncomingResource($item))->toArray($request);
        });

        return view('admin.transfer.transfer_incoming.include.pagination_data', compact('transfers'))->render();

    }


    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.incoming.index'), 'name' => __('Incoming transfer')],
            ['name' => __('Create')]
        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();

        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();


        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $usersSpecific = (new UserCollection($usersBranchDealer))->mainDetailsUser();

        $user = getUserAuthGuard();
        $source = getUserAgencyResource(get_class($user),$user);
        $transferID = generateRandomUniqueID();

        return view('admin.transfer.transfer_incoming.create', compact('transferID','currencies','source', 'countries', 'usersSpecific', 'breadcrumbs'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransferIncomingStoreRequest  $request)
    {

        $result = app('servicesV1')->transferIncomingService->storeTransfer($request);

        if ($result) {
            return redirect()->route('transfer.incoming.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('transfer.incoming.index')->with('failed', __('message.failed'));
        }
    }


    public function edit($id)
    {
        $result = app('servicesV1')->transferIncomingService->getTransferById($id);


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.incoming.index'), 'name' => __('Incoming transfer')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->transfer_id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $transfer = (new TransferIncomingResource($result))->toArray();
        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $countriesData = app('servicesV1')->locationService->getAllCountries(null);
        $countries = (new CountryCollection($countriesData))->toArray();
        $citiesData = app('servicesV1')->locationService->getCitiesByCountryId($result->country->id);
        $cities = (new CityCollection($citiesData))->toArray();
        $usersBranchDealer = app('servicesV1')->authenticationService->getBranchDealerTypeUsers();
        $usersSpecific = (new UserCollection($usersBranchDealer))->mainDetailsUser();
        $user = getUserAuthGuard();
        $source = getUserAgencyResource(get_class($user),$user);

        return view('admin.transfer.transfer_incoming.edit', compact('source','cities','usersSpecific', 'transfer', 'currencies', 'countries', 'breadcrumbs'));

    }


    public function show($id)
    {
        $result = app('servicesV1')->transferIncomingService->getTransferById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('transfer.incoming.index'), 'name' => __('Incoming transfer')],
            ['name' => __('Show')],
            ['name' => "#" . $result->transfer_id . '-' . $result->created_at->format('m/d/Y')]
        ];

        $transfer = (new TransferIncomingResource($result))->toArray();


        return view('admin.transfer.transfer_incoming.show', compact('transfer','breadcrumbs'));

    }

    public function update(TransferIncomingStoreRequest $request, $id){

        $result = app('servicesV1')->transferIncomingService->updateTransfer($request,$id);

        if ($result) {
            return redirect()->route('transfer.incoming.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('transfer.incoming.index')->with('failed', __('message.failed'));
        }
    }


    public function destroy($id)
    {

        $result = app('servicesV1')->transferIncomingService->destroyTransfer($id);

        if ($result) {
            return redirect()->route('transfer.incoming.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('transfer.incoming.index')->with('failed', __('message.failed'));
        }
    }

    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new IncomingTransferExport(), __('Incoming-Transfers') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('transfer.outgoing.index')->with('failed', __('message.failed'));
        }
    }
}
