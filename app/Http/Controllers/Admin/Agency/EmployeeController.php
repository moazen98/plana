<?php

namespace App\Http\Controllers\Admin\Agency;

use App\Exports\EmployeeExport;
use App\Exports\UserExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\Agency\Employee\EmployeeStoreRequest;
use App\Http\Requests\Admin\Agency\Employee\EmployeeUpdateRequest;
use App\Http\Requests\Admin\User\UserStoreRequest;
use App\Http\Requests\Admin\User\UserUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Company\CompanyCollection;
use App\Http\Resources\Dashboard\Agency\Company\CompanyResource;
use App\Http\Resources\Dashboard\Agency\Employee\EmployeeResource;
use App\Http\Resources\Dashboard\Agency\User\UserResource;
use App\Http\Resources\Dashboard\Role\RoleCollection;
use App\Models\Role;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_employee'])->only('index');
        $this->middleware(['permission:create_employee'])->only('create');
        $this->middleware(['permission:update_employee'])->only('edit');
        $this->middleware(['permission:delete_employee'])->only('destroy');
        $this->middleware(['permission:read_employee'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Employees')]
        ];

        $employees = app('servicesV1')->employeeService->getAllEmployee()->paginate($this->dashboardPaginate);

        $employees->getCollection()->transform(function ($item) use ($request) {
            return (new EmployeeResource($item))->toArray($request);
        });

        $companiesCollection = app('servicesV1')->companyService->getAllCompanies()->get();

        $companies = (new CompanyCollection($companiesCollection))->toArray();

        return view('admin.agency.employee.index', compact('companies','employees', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {


        $employees = app('servicesV1')->employeeService->filterEmployees($request)->paginate($this->dashboardPaginate);

        $employees->getCollection()->transform(function ($item) use ($request) {
            return (new EmployeeResource($item))->toArray($request);
        });

        return view('admin.agency.employee.include.pagination_data', compact('employees'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Add')]
        ];

        $companiesCollection = app('servicesV1')->companyService->getAllCompanies()->get();
        $companies = (new CompanyCollection($companiesCollection))->toArray();

        return view('admin.agency.employee.create', compact('companies', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {

        $employees = app('servicesV1')->employeeService->storeEmployee($request);

        if ($employees) {
            return redirect()->route('employee.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('employee.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = app('servicesV1')->employeeService->getEmployeeById($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . ' - ' . $result->first_name . ' ' . $result->last_name . ' - ' . $result->created_at->format('m/d/Y')]

        ];

        $employee = (new EmployeeResource($result))->toArray();

        return view('admin.agency.employee.show', compact('employee', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $result = app('servicesV1')->employeeService->getEmployeeById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('employee.index'), 'name' => __('Employees')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . ' - ' . $result->first_name . ' ' . $result->last_name . ' - ' . $result->created_at->format('m/d/Y')]

        ];


        $employee = (new EmployeeResource($result))->toArray();
        $companiesCollection = app('servicesV1')->companyService->getAllCompanies()->get();
        $companies = (new CompanyCollection($companiesCollection))->toArray();


        return view('admin.agency.employee.edit', compact('employee', 'companies', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->employeeService->updateEmployee($request, $id);

        if ($result) {
            return redirect()->route('employee.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('employee.index')->with('failed', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->employeeService->destroyEmployeeById($id);

        if ($result) {
            return redirect()->route('employee.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('employee.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        ob_end_clean();

        return Excel::download(new EmployeeExport(), __('Employees') . '.xlsx');
    }
}
