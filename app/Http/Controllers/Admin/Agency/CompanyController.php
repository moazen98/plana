<?php

namespace App\Http\Controllers\Admin\Agency;

use App\Exports\CompanyExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Requests\Admin\Agency\Company\AgencyStoreRequest;
use App\Http\Requests\Admin\Agency\Company\AgencyUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Company\CompanyResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CompanyController extends MainDashboardController
{
    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_company'])->only('index');
        $this->middleware(['permission:create_company'])->only('create');
        $this->middleware(['permission:update_company'])->only('edit');
        $this->middleware(['permission:delete_company'])->only('destroy');
        $this->middleware(['permission:read_company'])->only('export');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Companies')]
        ];

        $companies = app('servicesV1')->companyService->getAllCompanies()->paginate($this->dashboardPaginate);

        $companies->getCollection()->transform(function ($item) use ($request) {
            return (new CompanyResource($item))->toArray($request);
        });


        return view('admin.agency.company.index', compact('companies', 'breadcrumbs'));
    }


    public function fetchData(Request $request)
    {


        $companies = app('servicesV1')->companyService->filterCompanies($request)->paginate($this->dashboardPaginate);

        $companies->getCollection()->transform(function ($item) use ($request) {
            return (new CompanyResource($item))->toArray($request);
        });

        return view('admin.agency.company.include.pagination_data', compact('companies'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('company.index'), 'name' => __('Companies')],
            ['name' => __('Add')]
        ];

        return view('admin.agency.company.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgencyStoreRequest $request)
    {

        $employees = app('servicesV1')->companyService->storeCompany($request);

        if ($employees) {
            return redirect()->route('company.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('company.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $result = app('servicesV1')->companyService->getCompanyById($id);

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('Dashboard')],
            ['link' => route('company.index'), 'name' => __('Companies')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . ' - ' . $result->name . ' - ' . $result->created_at->format('m/d/Y')]

        ];

        $company = (new CompanyResource($result))->toArray();

        return view('admin.agency.company.show', compact('company', 'breadcrumbs'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $result = app('servicesV1')->companyService->getCompanyById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('company.index'), 'name' => __('Companies')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . ' - ' . $result->name . ' - ' . $result->created_at->format('m/d/Y')]

        ];


        $company = (new CompanyResource($result))->toArray();

        return view('admin.agency.company.edit', compact('company', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgencyUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->companyService->updateCompany($request, $id);

        if ($result) {
            return redirect()->route('company.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('company.index')->with('failed', __('message.failed'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = app('servicesV1')->companyService->destroyCompanyById($id);

        if ($result) {
            return redirect()->route('company.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('company.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        ob_end_clean();

        return Excel::download(new CompanyExport(), __('Companies') . '.xlsx');
    }
}
