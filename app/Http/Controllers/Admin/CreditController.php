<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CreditExport;
use App\Exports\IncomingTransferExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Credit\CreditStoreRequest;
use App\Http\Requests\Admin\Credit\CreditUpdateRequest;
use App\Http\Requests\Admin\Transfer\Incoming\TransferIncomingStoreRequest;
use App\Http\Resources\Dashboard\Agency\User\UserCollection;
use App\Http\Resources\Dashboard\Credit\CreditCollection;
use App\Http\Resources\Dashboard\Credit\CreditResource;
use App\Http\Resources\Dashboard\Credit\Status\CreditStatusCollection;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Operation\StatusCollection;
use App\Http\Resources\Dashboard\Transfer\Incoming\TransferIncomingResource;
use App\Http\Resources\Dashboard\Transfer\TransferTypeCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CreditController extends MainDashboardController
{

    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_credit'])->only('index');
        $this->middleware(['permission:create_credit'])->only('create');
        $this->middleware(['permission:update_credit'])->only('edit');
        $this->middleware(['permission:delete_credit'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Credits')]
        ];

        $credits = app('servicesV1')->creditService->getAllCredits()->paginate($this->dashboardPaginate);


        $credits->getCollection()->transform(function ($item) use ($request) {
            return (new CreditResource($item))->toArray($request);
        });




        $statusData = app('servicesV1')->creditService->creditStatus();
        $status = (new StatusCollection($statusData))->toArray($request);

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray($request);

        $usersData = app('servicesV1')->authenticationService->getAllTypeUsers();
        $users = (new UserCollection($usersData))->mainDetailsUser();

        return view('admin.credit.index', compact('credits', 'status','currencies','users', 'breadcrumbs'));
    }

    public function fetch_data(Request $request)
    {

        $credits = app('servicesV1')->creditService->filterCredits($request)->paginate($this->dashboardPaginate);


        $credits->getCollection()->transform(function ($item) use ($request) {
            return (new CreditResource($item))->toArray($request);
        });


        return view('admin.credit.include.pagination_data', compact('credits'))->render();

    }


    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('credit.index'), 'name' => __('Credits')],
            ['name' => __('Create')]
        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();


        $usersData = app('servicesV1')->authenticationService->getAllTypeUsers();
        $users = (new UserCollection($usersData))->mainDetailsUser();


        return view('admin.credit.create', compact('users','currencies','breadcrumbs'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreditStoreRequest $request)
    {

        $result = app('servicesV1')->creditService->storeCredit($request);

        if ($result) {
            return redirect()->route('credit.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('credit.index')->with('failed', __('message.failed'));
        }
    }


    public function edit($id)
    {
        $result = app('servicesV1')->creditService->getCreditById($id);


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('credit.index'), 'name' => __('Credits')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();

        $credit = (new CreditResource($result))->toArray();

        $usersData = app('servicesV1')->authenticationService->getAllTypeUsers();
        $users = (new UserCollection($usersData))->mainDetailsUser();


        return view('admin.credit.edit', compact('users','credit', 'currencies', 'breadcrumbs'));

    }


    public function show($id)
    {
        $result = app('servicesV1')->creditService->getCreditById($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('credit.index'), 'name' => __('Credits')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]
        ];

        $credit = (new CreditResource($result))->toArray();

        return view('admin.credit.show', compact('credit','breadcrumbs'));

    }

    public function update(CreditUpdateRequest $request, $id){

        $result = app('servicesV1')->creditService->updateCredit($request,$id);

        if ($result) {
            return redirect()->route('credit.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('credit.index')->with('failed', __('message.failed'));
        }
    }


    public function destroy($id)
    {

        $result = app('servicesV1')->creditService->destroyCredit($id);

        if ($result) {
            return redirect()->route('transfer.incoming.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('transfer.incoming.index')->with('failed', __('message.failed'));
        }
    }

    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new CreditExport(), __('Credits') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('credit.index')->with('failed', __('message.failed'));
        }
    }
}
