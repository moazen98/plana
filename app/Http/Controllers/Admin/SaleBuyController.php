<?php

namespace App\Http\Controllers\Admin;

use App\Exports\BranchExport;
use App\Exports\DealershipExport;
use App\Exports\SaleExport;
use App\Http\Controllers\Admin\MainDashboardController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Agency\Branch\BranchStoreRequest;
use App\Http\Requests\Admin\Agency\Branch\BranchUpdateRequest;
use App\Http\Requests\Admin\Agency\Dealership\DealershipStoreRequest;
use App\Http\Requests\Admin\Agency\Dealership\DealershipUpdateRequest;
use App\Http\Requests\Admin\Operation\OperationStoreRequest;
use App\Http\Requests\Admin\Operation\OperationUpdateRequest;
use App\Http\Resources\Dashboard\Agency\Branch\BranchResource;
use App\Http\Resources\Dashboard\Agency\Dealer\DealerResource;
use App\Http\Resources\Dashboard\Currency\CurrencyCollection;
use App\Http\Resources\Dashboard\Location\City\CityCollection;
use App\Http\Resources\Dashboard\Location\Country\CountryCollection;
use App\Http\Resources\Dashboard\Operation\OperationResource;
use App\Http\Resources\Dashboard\Operation\StatusCollection;
use App\Models\Agency\Dealership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;


class SaleBuyController extends MainDashboardController
{

    public function __construct(Request $request)
    {
        $this->dashboardPaginate = $request->has('page_counter') ? ($request->page_counter == 0 ? $this->dashboardPaginate : $request->page_counter) : $this->dashboardPaginate;

        //create read update delete
        $this->middleware(['permission:read_sale'])->only('index');
        $this->middleware(['permission:create_sale'])->only('create');
        $this->middleware(['permission:update_sale'])->only('edit');
        $this->middleware(['permission:delete_sale'])->only('destroy');
        $this->middleware(['permission:read_sale'])->only('export');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['name' => __('Sale/Buy')]
        ];

        $operations = app('servicesV1')->saleBuyService->getAllSales()->paginate($this->dashboardPaginate);


        $operations->getCollection()->transform(function ($item) use ($request) {
            return (new OperationResource($item))->toArray($request);
        });
        $statusData = app('servicesV1')->saleBuyService->getStatus();
        $status = (new StatusCollection($statusData))->toArray($request);


        return view('admin.operation.index', compact('operations', 'status', 'breadcrumbs'));
    }


    public function fetch_data(Request $request)
    {

        $operations = app('servicesV1')->saleBuyService->filterSales($request)->paginate($this->dashboardPaginate);

        $operations->getCollection()->transform(function ($item) use ($request) {
            return (new OperationResource($item))->toArray($request);
        });

        return view('admin.operation.include.pagination_data', compact('operations'))->render();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('sale.index'), 'name' => __('Sale/Buy')],
            ['name' => __('Create')]
        ];


        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();


        return view('admin.operation.create', compact('currencies', 'breadcrumbs'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OperationStoreRequest $request)
    {
        $result = app('servicesV1')->saleBuyService->storeOperation($request);

        if ($result) {
            return redirect()->route('sale.index')->with('success', __('message.success'));
        } else {
            return redirect()->route('sale.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = app('servicesV1')->saleBuyService->getSale($id);

        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('sale.index'), 'name' => __('Sale/Buy')],
            ['name' => __('Show')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $operation = (new OperationResource($result))->toArray();

        return view('admin.operation.show', compact('operation', 'breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = app('servicesV1')->saleBuyService->getSale($id);


        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('sale.index'), 'name' => __('Sale/Buy')],
            ['name' => __('Edit')],
            ['name' => "#" . $result->id . '-' . $result->created_at->format('m/d/Y')]

        ];

        $currenciesData = app('servicesV1')->currencyService->getAllCurrencies();
        $currencies = (new CurrencyCollection($currenciesData))->toArray();
        $operation = (new OperationResource($result))->toArray();


        return view('admin.operation.edit', compact('operation', 'currencies', 'breadcrumbs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OperationUpdateRequest $request, $id)
    {

        $result = app('servicesV1')->saleBuyService->updateOperation($request, $id);

        if ($result) {
            return redirect()->route('sale.index')->with('success', __('message.update'));
        } else {
            return redirect()->route('sale.index')->with('failed', __('message.failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = app('servicesV1')->saleBuyService->destroyOperation($id);

        if ($result) {
            return redirect()->route('sale.index')->with('success', __('message.delete'));
        } else {
            return redirect()->route('sale.index')->with('failed', __('message.failed'));
        }
    }


    public function export()
    {
        try {

            ob_end_clean();

            return Excel::download(new SaleExport(), __('sales-buys') . '.xlsx');
        } catch (\Exception $exception) {
            return redirect()->route('sale.index')->with('failed', __('message.failed'));
        }
    }
}
