<?php

namespace App\Http\Controllers\Admin\Website\MainPage\Publishing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\MainPage\About\AboutStoreRequest;
use App\Http\Requests\Admin\Website\MainPage\About\AboutUpdateRequest;
use App\Models\AboutUsPublisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublishingController extends Controller
{

    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_about_publisher'])->only('index');
        $this->middleware(['permission:create_about_publisher'])->only('create');
        $this->middleware(['permission:update_about_publisher'])->only('edit');
        $this->middleware(['permission:delete_about_publisher'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['name' => __('About Us')]
        ];


        $aboutUs = AboutUsPublisher::first();


        return view('admin.website.mainPage.aboutUs.index', compact('aboutUs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/about-publishing/index", 'name' => __('About Us')],
            ['name' => __('Create About Us')]
        ];

        return view('admin.website.mainPage.aboutUs.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutStoreRequest $request)
    {
        $aboutUs = new AboutUsPublisher();
        $aboutUs->description = $request->description;
        $aboutUs->user()->associate(Auth::user());

        $aboutUs->save();

        return redirect()->route('website.main.publishing.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/about-publishing/index", 'name' => __('About Us')],
            ['name' => __('Edit About Us')]
        ];

        $aboutUs = AboutUsPublisher::find($id);

        return view('admin.website.mainPage.aboutUs.edit', compact('breadcrumbs', 'aboutUs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUpdateRequest $request, $id)
    {
        $aboutUs = AboutUsPublisher::find($id);
        $aboutUs->description = $request->description;
        $aboutUs->user()->associate(Auth::user());
        $aboutUs->save();

        return redirect()->route('website.main.publishing.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aboutUs = AboutUsPublisher::find($id);
        $aboutUs->delete();

        return redirect()->route('website.main.publishing.index')->with('success', __('message.delete'));
    }
}
