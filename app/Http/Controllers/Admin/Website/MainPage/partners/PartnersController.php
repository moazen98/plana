<?php

namespace App\Http\Controllers\Admin\Website\MainPage\partners;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\MainPage\Partner\PartnerStoreRequest;
use App\Http\Requests\Admin\Website\MainPage\Partner\PartnerUpadteRequest;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PartnersController extends Controller
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_our_partner'])->only('index');
        $this->middleware(['permission:create_our_partner'])->only('create');
        $this->middleware(['permission:update_our_partner'])->only('edit');
        $this->middleware(['permission:delete_our_partner'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['name' => __('Our Partners Page')]
        ];

        $partners = Partner::paginate(10);

        return view('admin.website.mainPage.partner.index', compact('partners', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/our-partners/index", 'name' => __('Our Partner')],
            ['name' => __('Add Partner')]
        ];

        return view('admin.website.mainPage.partner.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartnerStoreRequest $request)
    {
        $partner = new Partner();
        $partner->description = $request->description;
        $partner->title = $request->title;
        $partner->url = $request->url;
        $partner->user()->associate(Auth::user());


        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_partner'), 'partner', 'jpeg|jpg|png', $request);

            $partner->image_url = Config::get('doc.website_partner') . $files[0]['real_path'];

        } else {
            $partner->image_url = 'website/img/logo.svg';
        }

        $partner->save();

        return redirect()->route('website.main.partner.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/main-page/index", 'name' => __('Main Page')],
            ['link' => "/admin/website-pages/main-page/our-partners/index", 'name' => __('Our Partner')],
            ['name' => __('Edit Partner')]
        ];

        $partner = Partner::findOrFail($id);

        return view('admin.website.mainPage.partner.edit', compact('partner', 'breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PartnerUpadteRequest $request, $id)
    {
        $partner = Partner::find($id);
        $partner->description = $request->description;
        $partner->title = $request->title;
        $partner->url = $request->url;
        $partner->user()->associate(Auth::user());


        if ($request->has('image')) {
            $files = upload_files(Config::get('doc.website_partner'), 'partner', 'jpeg|jpg|png', $request);

            $partner->image_url = Config::get('doc.website_partner') . $files[0]['real_path'];

        }

        $partner->save();

        return redirect()->route('website.main.partner.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::findOrFail($id);
        $partner->delete();

        return redirect()->route('website.main.partner.index')->with('success', __('message.delete'));
    }
}
