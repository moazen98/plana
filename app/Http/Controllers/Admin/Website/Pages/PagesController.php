<?php

namespace App\Http\Controllers\Admin\Website\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['name' => __('Pages')]
        ];


        return view('admin.website.pages.index',compact('breadcrumbs'));
    }
}
