<?php

namespace App\Http\Controllers\Admin\Website\Pages\ContactInformation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\ContactInformation\ContactInformationStoreRequest;
use App\Http\Requests\Admin\Website\Pages\ContactInformation\ContactInformationUpdateRequest;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactInformationController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_contact_us'])->only('index');
        $this->middleware(['permission:create_contact_us'])->only('create');
        $this->middleware(['permission:update_contact_us'])->only('edit');
        $this->middleware(['permission:delete_contact_us'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Contact Us')]
        ];

        $contactUs = ContactUs::all();

        return view('admin.website.pages.contactUs.index', compact('contactUs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Create Contact Us')]
        ];

        return view('admin.website.pages.contactUs.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactInformationStoreRequest $request)
    {
        $contactUs = new ContactUs();
        $contactUs->description = $request->description;
        $contactUs->user()->associate(Auth::user());

        $contactUs->save();

        return redirect()->route('website.page.contact.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/pages/index", 'name' => __('Pages')],
            ['name' => __('Edit Contact Us')]
        ];

        $contactUs = ContactUs::find($id);

        return view('admin.website.pages.contactUs.edit', compact('breadcrumbs', 'contactUs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactInformationUpdateRequest $request, $id)
    {
        $contactUs = ContactUs::find($id);
        $contactUs->description = $request->description;
        $contactUs->user()->associate(Auth::user());
        $contactUs->save();

        return redirect()->route('website.page.contact.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contantUs = ContactUs::find($id);
        $contantUs->delete();

        return redirect()->route('website.page.contact.index')->with('success', __('message.delete'));
    }
}
