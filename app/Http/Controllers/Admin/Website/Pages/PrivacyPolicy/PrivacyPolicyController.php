<?php

namespace App\Http\Controllers\Admin\Website\Pages\PrivacyPolicy;

use App\Enums\UsageInformationType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyStoreRequest;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyUpdateRequest;
use App\Http\Resources\Dashboard\UsageInformation\UsageResource;

class PrivacyPolicyController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_privacy_policy'])->only('index');
        $this->middleware(['permission:create_payment_policy'])->only('create');
        $this->middleware(['permission:update_privacy_policy'])->only('edit');
        $this->middleware(['permission:delete_privacy_policy'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Privacy and Policy')]
        ];


        $data = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::PRIVACY);

        $privacy = (new UsageResource($data))->toArray();

        return view('admin.website.pages.privacy.index', compact('privacy', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Create Privacy and Policy')]
        ];

        return view('admin.website.pages.privacy.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyStoreRequest $request)
    {

        app('servicesV1')->usageInformationService->storeUsageInformation(UsageInformationType::PRIVACY, $request);

        return redirect()->route('website.page.privacy.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Edit Privacy and Policy')]
        ];

        $data = app('servicesV1')->usageInformationService->editUsageInformation(UsageInformationType::PRIVACY);

        $privacy = (new UsageResource($data))->toArray();

        return view('admin.website.pages.privacy.edit', compact('breadcrumbs', 'privacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyStoreRequest $request)
    {
        app('servicesV1')->usageInformationService->updateUsageInformation((string)UsageInformationType::PRIVACY, $request);

        return redirect()->route('website.page.privacy.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        app('servicesV1')->usageInformationService->deleteUsageInformation(UsageInformationType::PRIVACY);

        return redirect()->route('website.page.privacy.index')->with('success', __('message.delete'));
    }
}
