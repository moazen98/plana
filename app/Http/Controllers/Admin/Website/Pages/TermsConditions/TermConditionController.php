<?php

namespace App\Http\Controllers\Admin\Website\Pages\TermsConditions;

use App\Enums\UsageInformationType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyStoreRequest;
use App\Http\Resources\Dashboard\UsageInformation\UsageResource;

class TermConditionController extends Controller
{

    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_terms_conditions'])->only('index');
        $this->middleware(['permission:create_terms_conditions'])->only('create');
        $this->middleware(['permission:update_terms_conditions'])->only('edit');
        $this->middleware(['permission:delete_terms_conditions'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Terms and Conditions')]
        ];


        $data = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::TERMS);

        $term = (new UsageResource($data))->toArray();

        return view('admin.website.pages.term.index', compact('term', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Add Terms and Conditions')]
        ];

        return view('admin.website.pages.term.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyStoreRequest $request)
    {

        app('servicesV1')->usageInformationService->storeUsageInformation(UsageInformationType::TERMS, $request);

        return redirect()->route('website.page.term.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Edit Terms and Conditions')]
        ];

        $data = app('servicesV1')->usageInformationService->editUsageInformation(UsageInformationType::TERMS);

        $term = (new UsageResource($data))->toArray();

        return view('admin.website.pages.term.edit', compact('breadcrumbs', 'term'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyStoreRequest $request)
    {
        app('servicesV1')->usageInformationService->updateUsageInformation((string)UsageInformationType::TERMS, $request);

        return redirect()->route('website.page.term.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        app('servicesV1')->usageInformationService->deleteUsageInformation(UsageInformationType::TERMS);

        return redirect()->route('website.page.term.index')->with('success', __('message.delete'));
    }
}
