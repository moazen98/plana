<?php

namespace App\Http\Controllers\Admin\Website\Pages\AboutUs;

use App\Enums\UsageInformationType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Pages\PrivacyPolicy\PrivacyPolicyStoreRequest;
use App\Http\Resources\Dashboard\UsageInformation\UsageResource;

class AboutUsController extends Controller
{
    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_about_us'])->only('index');
        $this->middleware(['permission:create_about_us'])->only('create');
        $this->middleware(['permission:update_about_us'])->only('edit');
        $this->middleware(['permission:delete_about_us'])->only('destroy');

    }//end of constructor

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('About Us')]
        ];


        $data = app('servicesV1')->usageInformationService->getUsageInformation(UsageInformationType::ABOUT_US);

        $about = (new UsageResource($data))->toArray();

        return view('admin.website.pages.about.index', compact('about', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('About Us')]
        ];

        return view('admin.website.pages.about.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrivacyPolicyStoreRequest $request)
    {

        app('servicesV1')->usageInformationService->storeUsageInformation(UsageInformationType::ABOUT_US, $request);

        return redirect()->route('website.page.about.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $breadcrumbs = [
            ['link' => route('admin.dashboard-ecommerce'), 'name' => __('dashboard')],
            ['link' => route('website.page.index'), 'name' => __('Pages')],
            ['name' => __('Edit About Us')]
        ];

        $data = app('servicesV1')->usageInformationService->editUsageInformation(UsageInformationType::ABOUT_US);

        $about = (new UsageResource($data))->toArray();

        return view('admin.website.pages.about.edit', compact('breadcrumbs', 'about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrivacyPolicyStoreRequest $request)
    {
        app('servicesV1')->usageInformationService->updateUsageInformation((string)UsageInformationType::ABOUT_US, $request);

        return redirect()->route('website.page.about.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        app('servicesV1')->usageInformationService->deleteUsageInformation(UsageInformationType::ABOUT_US);

        return redirect()->route('website.page.about.index')->with('success', __('message.delete'));
    }
}
