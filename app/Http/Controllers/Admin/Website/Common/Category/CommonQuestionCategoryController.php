<?php

namespace App\Http\Controllers\Admin\Website\Common\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Common\Category\CommonCategoryStoreRequestue;
use App\Http\Requests\Admin\Website\Common\Category\CommonCategoryUpdateRequestue;
use App\Models\QuestionCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonQuestionCategoryController extends Controller
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_question_categories'])->only('index');
        $this->middleware(['permission:create_question_categories'])->only('create');
        $this->middleware(['permission:update_question_categories'])->only('edit');
        $this->middleware(['permission:delete_question_categories'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['name' => __('Questions Categories')]
        ];

        $categories = QuestionCategory::when($request->search, function ($q) use ($request) {
            return $q->where('name_ar', 'like', '%' . $request->search . '%')
                ->orWhere('name_en', 'like', '%' . $request->search . '%');
        })->latest()->paginate(10);


        return view('admin.website.common.category.index', compact('categories', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/common-questions-category/index", 'name' => __('Questions Categories')],
            ['name' => __('Create')]
        ];

        return view('admin.website.common.category.create', compact('breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommonCategoryStoreRequestue $request)
    {
        $category = new QuestionCategory();
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;

        if ($request->has('active')) {
            $category->active = 1;
        } else {
            $category->active = 0;
        }

        $category->user()->associate(Auth::user());

        $category->save();

        return redirect()->route('website.common.category.index')->with('success', __('message.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/common-questions-category/index", 'name' => __('Questions Categories')],
            ['name' => __('Show')]
        ];

        $category = QuestionCategory::find($id);

        return view('admin.website.common.category.show', compact('category','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/common-questions-category/index", 'name' => __('Questions Categories')],
            ['name' => __('Edit')]
        ];

        $category = QuestionCategory::find($id);

        return view('admin.website.common.category.edit', compact('category','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommonCategoryUpdateRequestue $request, $id)
    {
        $category = QuestionCategory::find($id);
        $category->name_ar = $request->name_ar;
        $category->name_en = $request->name_en;
        $category->name_en = $request->name_en;

        if ($request->has('active')) {
            $category->active = 1;
        } else {
            $category->active = 0;
        }

        $category->user()->associate(Auth::user());

        $category->save();

        return redirect()->route('website.common.category.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = QuestionCategory::find($id);

        $category->delete();

        return redirect()->route('website.common.category.index')->with('success', __('message.delete'));
    }
}
