<?php

namespace App\Http\Controllers\Admin\Website\Common\Question;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Website\Common\Question\CommonQuestionStoreRequest;
use App\Http\Requests\Admin\Website\Common\Question\CommonQuestionUpdateRequest;
use App\Models\CommonQuestion;
use App\Models\QuestionCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonQuestionController extends Controller
{


    public function __construct()
    {
        //create read update delete
        $this->middleware(['permission:read_question'])->only('index');
        $this->middleware(['permission:create_question'])->only('create');
        $this->middleware(['permission:update_question'])->only('edit');
        $this->middleware(['permission:delete_question'])->only('destroy');

    }//end of constructor


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['name' => __('Common Questions')]
        ];


        $questions = CommonQuestion::when($request->search, function ($q) use ($request) {
            return $q->where('question_ar', 'like', '%' . $request->search . '%')
                ->orWhere('question_en', 'like', '%' . $request->search . '%')
                ->orWhere('answer_ar', 'like', '%' . $request->search . '%')
                ->orWhere('answer_en', 'like', '%' . $request->search . '%');
        })->latest()->paginate(10);

        return view('admin.website.common.question.index', compact('questions', 'breadcrumbs'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/common-questions/index", 'name' => __('Common Questions')],
            ['name' => __('Create')]
        ];

        $categories = QuestionCategory::all()->where('active', '=', 1);

        return view('admin.website.common.question.create', compact('categories', 'breadcrumbs'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommonQuestionStoreRequest $request)
    {
        $question = new CommonQuestion();
        $question->question_ar = $request->question_ar;
        $question->question_en = $request->question_en;
        $question->answer_ar = $request->answer_ar;
        $question->answer_en = $request->answer_en;

        $category = QuestionCategory::find($request->category);
        $question->user()->associate(Auth::user());
        $question->category()->associate($category);

        $question->save();

        return redirect()->route('website.common.question.index')->with('success', __('message.success'));


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumbs = [
            ['link' => "/admin/dashboard", 'name' => __('dashboard')],
            ['link' => "/admin/website-pages/common-questions/index", 'name' => __('Common Questions')],
            ['name' => __('Edit')]
        ];

        $categories = QuestionCategory::all()->where('active', '=', 1);


        $question = CommonQuestion::find($id);

        return view('admin.website.common.question.edit', compact('question', 'categories', 'breadcrumbs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommonQuestionUpdateRequest $request, $id)
    {
        $question = CommonQuestion::find($id);
        $question->question_ar = $request->question_ar;
        $question->question_en = $request->question_en;
        $question->answer_ar = $request->answer_ar;
        $question->answer_en = $request->answer_en;

        $category = QuestionCategory::find($request->category);
        $question->user()->associate(Auth::user());
        $question->category()->associate($category);

        $question->save();

        return redirect()->route('website.common.question.index')->with('success', __('message.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = CommonQuestion::find($id);

        $question->delete();

        return redirect()->route('website.common.question.index')->with('success', __('message.delete'));
    }
}
