<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class MainDashboardController extends Controller
{

    protected $dashboardPaginate = 10;

    public function __construct()
    {
        $dashboardPaginate = config('custom_settings.dashboard_paginate');
    }
}
