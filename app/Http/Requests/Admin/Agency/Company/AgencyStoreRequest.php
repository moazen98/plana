<?php

namespace App\Http\Requests\Admin\Agency\Company;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class AgencyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return RuleFactory::make([
            '%name%' => ['required'],
            'email' => 'nullable|email',
            'website' => 'nullable|url',
            'files.*' => 'mimes:jpg,jpeg,png',
        ]);
    }

    public function attributes()
    {
        return RuleFactory::make([
            '%name%' => __('validation.attributes.name'),
        ]);
    }


    public function messages()
    {
        return [
            '*.name.required' => __('validation.required'),
            'email.email' => __('validation.email'),
            'website.url' => __('validation.url'),
            'files.*.mimes' => __('validation.mimes'),
        ];
    }
}
