<?php

namespace App\Http\Requests\Admin\Agency\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:190',
            'last_name' => 'required|max:190',
            'phone' => 'nullable|numeric',
            'international_code' => 'nullable',
            'email' => 'nullable|email|max:190',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => __('validation.required'),
            'first_name.max' => __('validation.max_string'),
            'last_name.required' => __('validation.required'),
            'last_name.max' => __('validation.max_string'),
            'phone.numeric' => trans('validation.numeric'),
            'email.email' => __('validation.email'),
            'email.max' => __('validation.max_string'),
        ];
    }
}
