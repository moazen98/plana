<?php

namespace App\Http\Requests\Admin\User\Role;

use Illuminate\Foundation\Http\FormRequest;

class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ar' => 'required|unique:roles,name_ar,' . $this->id,
            'name' => 'required|unique:roles,name,' . $this->id,
            'description_ar' => 'required',
            'description' => 'required',
            'permissions' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_ar.required' => __('validation.required'),
            'name_ar.unique' => __('validation.unique'),
            'name.required' => __('validation.required'),
            'name.unique' => __('validation.unique'),
            'description_ar.required' => __('validation.required'),
            'description.required' => __('validation.required'),
            'permissions.required' => __('validation.required'),
        ];
    }
}
