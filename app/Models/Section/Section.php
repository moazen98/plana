<?php

namespace App\Models\Section;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;

class Section extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SoftDeletes;

    protected $table = 'sections';
    protected $fillable = ['description','active','image_url'];
    public $translatedAttributes = ['name'];
    protected $appends = ['image_path'];

    public function employees(){
        return $this->hasMany(User::class,'section_id','id');
    }

    public function getImagePathAttribute()
    {
        return asset('storage/section/' . $this->image_url);

    }//end of get image path

    public function setTranslatedAttributes($request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->name = $request->get($locale)['name'];
            }

        }
    }


    public function setImage_urlAttributes(Request $request)
    {

        if ($request->has('image')) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path(Config::get('custom_settings.image_section') . $request->image->hashName()));

            $this->attributes['image_url'] = $request->image->hashName();
        } else {

            $this->attributes['image_url'] = Config::get('custom_settings.image_section_default');

        }
    }
}
