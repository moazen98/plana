<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'employees';
    protected $fillable = ['first_name','last_name','email','phone','international_code','company_id'];


    protected $appends = [
        'name',
        'full_phone'
    ];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;

    }

    public function getFullPhoneAttribute()
    {
        return $this->international_code . ' ' . $this->phone;

    }

    public function company(){
        return $this->belongsTo(Company::class,'company_id','id');
    }
}
