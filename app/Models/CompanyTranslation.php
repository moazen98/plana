<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyTranslation extends Model
{
    use HasFactory;

    protected $table = 'company_translations';
    protected $fillable = ['name'];
    public $timestamps = false;
}
