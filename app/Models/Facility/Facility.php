<?php

namespace App\Models\Facility;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
    use HasFactory, softDeletes, Translatable;

    protected $fillable = [
        'parent_id',
        'phone',
        'logo',
        'country',
        'service_start_date',
        'service_end_date',
        'services_provided',
        'subscription_is_active'

    ];

    public $translatedAttributes = [
        'name',
        'address'
    ];


    public function children()
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(__CLASS__, 'parent_id', 'id');
    }
}
