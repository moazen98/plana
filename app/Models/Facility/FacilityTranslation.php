<?php

namespace App\Models\Facility;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityTranslation extends Model
{
    use HasFactory, softDeletes;

    protected $fillable = [
        'id',
        'facility_id',
        'locale',
        'name',
        'address'
    ];


}
