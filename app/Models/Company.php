<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class Company extends Model implements TranslatableContract
{
    use HasFactory;
    use SoftDeletes;
    use Translatable;

    protected $table = 'companies';
    protected $fillable = ['email', 'website'];
    public $translatedAttributes = ['name'];


    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id', 'id');
    }

    public function file()
    {
        return $this->morphOne(File::class, 'fileable');
    }


    public function storeFiles(Request $request)
    {

        if ($request->has('files')) {


            $filesRequest = $request->all()['files'];


            $files = upload_single_files(Config::get('custom_settings.media_company'), $this->id, 'png|jpg|jpeg|pdf|xls', $filesRequest);

            $data = $files[0];

            $this->file()->updateOrCreate(
                ['id' => $this->file == null ? null : $this->file->id],
                [
                    'url' => $data['file_name'],
                    'real_name' => $data['real_name'],
                    'size' => $data['size'],
                    'extension_id' => MediaExtension::where('extension', $data['file_type'])->first() == null ? MediaExtension::first()->id : MediaExtension::where('extension', $data['file_type'])->first()->id,
                ]);

        } else {


            $this->file()->updateOrCreate([
                'url' => Config::get('custom_settings.image_company_default'),
                'real_name' => Config::get('custom_settings.image_company_default'),
                'size' => 50,
                'extension_id' => MediaExtension::where('extension', 'jpg')->first()->id,
            ]);
        }
    }

    public function setTranslatedAttributes($request)
    {
        foreach (config('translatable.locales') as $locale) {
            if ($request->has($locale)) {
                $this->getTranslationOrNew($locale)->name = $request->get($locale)['name'];
            }

        }
    }
}
