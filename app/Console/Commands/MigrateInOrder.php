<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateInOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:migrate_in_order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $migrations = [
            '2019_8_12_000000_create_sections_table.php',
            '2019_08_19_000000_create_failed_jobs_table.php',
            '2019_12_14_000001_create_personal_access_tokens_table.php',
            '2022_01_30_140219_create_cities_table.php',
            '2022_01_30_140235_create_areas_table.php',
            '2022_1_29_200000_create_users_table.php',
//            '2022_01_30_140246_create_customers_table.php',
//            '2022_01_30_140710_create_medias_table.php',
//            '2022_01_30_190322_create_stages_table.php',
//            '2022_02_02_074116_create_items_table.php',
//            '2022_01_30_193313_create_categories_table.php',
//            '2022_01_30_192404_create_services_table.php',
//            '2022_01_30_190434_create_orders_table.php',
//            '2022_02_09_103126_create_media_prescriptions_services.php',
//            '2022_02_04_184153_create_sessions_table.php',
//            '2022_01_30_191032_create_order_stage_table.php',
//            '2022_02_07_192159_create_programmes_table.php',
//            '2022_01_30_193100_create_notes_table.php',
//            '2022_01_30_191914_create_prescriptions_table.php',
//            '2022_02_06_084338_create_items_custom_fields_table.php',
//            '2022_02_06_085137_create_items_custom_fields_values_table.php',
//            '2022_02_06_130544_create_facecare_parameters_table.php',
//            '2022_02_06_130627_create_laser_parameters_table.php',
//            '2022_02_06_132742_create_category_custom_fields_table.php',
//            '2022_02_06_132804_create_category_custom_fields_values_table.php',
//            '2022_02_09_103126_create_media_prescriptions_services.php',
//            '2022_02_15_080225_create_bills_table.php',
//            '2022_02_15_093222_create_number_translate_table.php',
//            '2022_05_15_133935_create_companies_table.php',
//            '2022_05_15_134045_create_materials_table.php',
//            '2022_03_01_183259_create_pharmaceuticals_table.php',
//            '2022_03_02_075218_create_goods_table.php',
//            '2022_03_02_091630_create_recipe_table.php',
//            '2022_03_02_091630_create_recipe_table.php',
//            '2022_03_02_092328_create_pharmaceutical_recipe_table.php',
//            '2022_03_06_114828_create_diagnosis_table.php',
//            '2022_03_09_142453_create_treatment_programme_table.php',
//            '2022_03_15_152848_create_marketing_table.php',
//            '2022_03_16_132952_create_marketers_table.php',
//            '2022_03_27_210933_create_questionnaires_table.php',
//            '2022_04_03_095000_create_subscriptions_table.php',
//            '2022_04_06_144222_create_users_reviews_table.php',
//            '2022_04_07_092144_create_user_password_resets_table.php',
//            '2022_04_09_214057_create_holidays_table.php',
//            '2022_04_10_095244_create_messages_table.php',
//            '2022_04_11_153105_create_sliders_table.php',
//            '2022_05_16_121258_create_mobile_sections_table.php',
//            '2022_04_12_102010_create_mobile_services_table.php',
//            '2022_04_12_120025_create_advertisements_table.php',
//            '2022_04_13_095113_create_policies_table.php',
//            '2022_04_13_120922_create_customers_mobile_table.php',
//            '2022_04_14_123255_create_contact_us_table.php',
//            '2022_04_14_133653_create_address_table.php',
//            '2022_04_17_100427_create_mobile_orders_table.php',
//            '2022_05_15_133935_create_companies_table.php',
//            '2022_05_17_100735_create_insert_materials_table.php',
//            '2022_05_17_131205_create_pull_materials_table.php',
//            '2022_05_17_155339_create_about_us_table.php',
//            '2022_05_21_182428_create_financials_table.php',
//            '2022_05_21_225156_create_financials_company_table.php',
//            '2022_05_24_105527_create_financials_employee_table.php'
        ];

        foreach($migrations as $migration)
        {
            $basePath = 'database/migrations/';
            $migrationName = trim($migration);
            $path = $basePath.$migrationName;
            $this->call('make:create', [
                '--path' => $path ,
            ]);
        }
    }
}
