<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyService.
 */
class CompanyService extends MainDashboardService
{

    public function getAllCompanies()
    {

        $companies = Company::query()->latest();

        return $companies;
    }


    public function storeCompany($request)
    {

        try {
            DB::beginTransaction();

            $company = new Company();
            $company->email = $request->email;
            $company->website = $request->website;
            $company->setTranslatedAttributes($request);
            $company->save();

            $company->storeFiles($request);

            DB::commit();

            return true;

        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function updateCompany($request, $id)
    {

        try {


            DB::beginTransaction();

            $company = Company::query()->findOrFail($id);
            $company->email = $request->email;
            $company->website = $request->website;
            $company->setTranslatedAttributes($request);

            if ($request->has('files')) {
                $company->storeFiles($request);
            }

            $company->save();


            DB::commit();

            return true;


        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function getCompanyById($id)
    {
        $company = Company::query()->findOrFail($id);

        return $company;
    }

    public function destroyCompanyById($id)
    {

        DB::beginTransaction();

        //TODO: there is observer for it

        $company = Company::query()->findOrFail($id);
        $company->delete();

        DB::commit();

        return true;
    }


    public function filterCompanies($request)
    {


        $companies = Company::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $companies->where(function ($query) use ($search) {
                $query->whereTranslationLike('name', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('id', 'like', '%' . $search . '%');
            });
        }


        $orderBy = $dataTableFilter['sorttype'] ? $dataTableFilter['sorttype'] : 'DESC';
        $companies = $companies->orderBy('id', $orderBy);

        return $companies;
    }


}
