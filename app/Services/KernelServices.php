<?php

namespace App\Services;

/**
 * Class KernelServices.
 */
class KernelServices
{

    public $authenticationService;
    public $userService;
    public $companyService;
    public $employeeService;


    /**
     * KernelServices constructor.
     */
    public function __construct()
    {
        $this->authenticationService = new AuthenticationService();
        $this->userService = new UserService();
        $this->companyService = new CompanyService();
        $this->employeeService = new EmployeeService();
    }
}
