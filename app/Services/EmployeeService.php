<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\DB;

/**
 * Class EmployeeService.
 */
class EmployeeService extends MainDashboardService
{

    public function getAllEmployee()
    {

        $users = Employee::latest();

        return $users;
    }


    public function storeEmployee($request)
    {

//        try {

        DB::beginTransaction();

        $employee = new Employee();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->international_code = $request->international_code;
        $company = Company::findOrFail($request->company_id);
        $employee->company()->associate($company);
        $employee->save();

        DB::commit();

        return true;

//        } catch (\Exception $exception) {
//
//            DB::rollBack();
//
//            return false;
//        }

    }


    public function updateEmployee($request, $id)
    {

        try {


            $employee = Employee::query()->findOrFail($id);
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->international_code = $request->international_code;
            $company = Company::findOrFail($request->company_id);
            $employee->company()->associate($company);
            $employee->save();

            return true;


        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function getEmployeeById($id)
    {
        $employee = Employee::query()->findOrFail($id);

        return $employee;
    }

    public function destroyEmployeeById($id)
    {

        DB::beginTransaction();

        //TODO: there is observer for it

        $employee = Employee::query()->findOrFail($id);
        $employee->delete();

        DB::commit();

        return true;
    }


    public function filterEmployees($request)
    {


        $employees = Employee::query();

        $dataTableFilter = $request->all();


        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $employees->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhere('phone', 'like', '%' . $search . '%')
                    ->orWhereRaw("concat(first_name, ' ',  last_name) like '%" . $search . "%' ")
                    ->orWhere('id', 'like', '%' . $search . '%');
            });
        }


        if ($dataTableFilter['company_id']) {

            $company = $request->get('company_id');

            $employees->whereHas('company', function ($query) use ($company) {
                $query->where('id', $company);
            });

        }


        $orderBy = $dataTableFilter['sorttype'] ? $dataTableFilter['sorttype'] : 'DESC';
        $employees = $employees->orderBy('id', $orderBy);

        return $employees;
    }

}
