<?php

namespace App\Services;

use App\Models\MediaExtension;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class UserService.
 */
class UserService extends MainDashboardService
{

    public function getAllUsers()
    {

        $users = User::latest();

        return $users;
    }


    public function storeUser($request)
    {

        try {

            DB::beginTransaction();

            $employee = new User();
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->basic_user = 0;

            $employee->setImage_urlAttributes($request);
            $employee->save();

            $this->storeCredentials($employee, $request);

            $role = Role::findOrFail($request->role);
            $employee->attachRole($role);

            DB::commit();

            return true;

        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }

    public function storeCredentials($user, $request)
    {

        if ($request->password) {

            $credentials = $user->authentication()->updateOrCreate(
                [
                    'id' => $user->authentication ? $user->authentication->id : null,
                ],
                [
                    'phone' => $request->phone_number,
                    'email' => $request->email,
                    'international_code' => $request->international_code,
                    'password' => $request->password,
                    'is_active' => $request->has('is_active') ? 1 : 0,
                    'is_verified' => 1,
                ]);

        } else {
            $credentials = $user->authentication()->updateOrCreate(
                [
                    'id' => $user->authentication ? $user->authentication->id : null,
                ],
                [
                    'phone' => $request->phone_number,
                    'email' => $request->email,
                    'international_code' => $request->international_code,
                    'is_active' => $request->has('is_active') ? 1 : 0,
                    'is_verified' => 1,
                ]);
        }

        return $credentials;
    }


    public function updateUser($request, $id)
    {

        try {


            DB::beginTransaction();

            $employee = User::findOrFail($id);
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->basic_user = 0;
            $this->storeCredentials($employee, $request);

            if ($request->has('image')) {
                $employee->setImage_urlAttributes($request);
            }

            $role = Role::find($request->role);
            foreach ($employee->roles as $employeeRole) {
                $employee->detachRole($employeeRole);
            }

            $employee->attachRole($role);

            $employee->save();


            DB::commit();

            return true;


        } catch (\Exception $exception) {

            DB::rollBack();

            return false;
        }

    }


    public function getUserById($id)
    {
        $user = User::findOrFail($id);

        return $user;
    }

    public function destroyUserById($id)
    {

        DB::beginTransaction();

        //TODO: there is observer for it

        $user = User::findOrFail($id);
        $user->delete();

        DB::commit();

        return true;
    }


    public function filterUsers($request)
    {


        $users = User::query();

        $dataTableFilter = $request->all();



        if ($dataTableFilter['query']) {
            $search = $request->get('query');
            $users->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhereRaw("concat(first_name, ' ',  last_name) like '%" . $search . "%' ")
                    ->orWhere('id', 'like', '%' . $search . '%');
            })->orWhere(function ($query2) use ($search) {
                $query2->whereHas('authentication', function ($query3) use ($search) {
                    $query3->where('email', 'like', '%' . $search . '%')->orWhere('phone', 'like', '%' . $search . '%');
                });
            });
        }


        if ($dataTableFilter['status_id'] && $dataTableFilter['status_id'] != 0) {

            $status = $request->get('status_id');

            $users->whereHas('authentication', function ($query) use ($status) {
                $query->where('is_active', $status);
            });

        }


        if ($dataTableFilter['role_id']) {

            $role = $request->get('role_id');

            $users->whereHas('roles', function ($query) use ($role) {
                $query->where('id',$role);
            });

        }


        $orderBy = $dataTableFilter['sorttype'] ? $dataTableFilter['sorttype'] : 'DESC';
        $users = $users->orderBy('id', $orderBy);

        return $users;
    }

    public function getAllRoles(){

        $roles = Role::latest();
        return $roles;
    }



}
