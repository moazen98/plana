<?php

namespace App\Exports;

use App\Models\Employee;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements FromArray, WithHeadings
{
    public function array(): array
    {

        $data = array();

        $employees = Employee::all();

        foreach ($employees as $index => $employee) {
            $data[$index]['id'] = $employee->id;
            $data[$index]['first_name'] = $employee->first_name;
            $data[$index]['last_name'] = $employee->last_name;
            $data[$index]['phone'] = $employee->international_code . $employee->phone;
            $data[$index]['email'] = $employee->email;
            $data[$index]['date'] = $employee->created_at->format('m-d-Y');

        }

        return $data;
    }

    public function headings(): array
    {
        return [__("Id"), __("First Name"), __("Last Name"), __("Phone"), __("Email"), __("Date")];
    }
}
