<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MediaFor extends Enum
{
    const PHOTOGRAPH = 1;
    const FILES = 2;
    const OTHER = 3;
}
