<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class MediaExtension extends Enum
{
    const PNG ='png';
    const JPEG ='jpeg';
    const JPG ='jpg';
    const GIF = 'gif';
    const MP3 = 'mp3';
    const WAV = 'wav';
    const MP4 = 'mp4';
    const OGG = 'ogg';
    const MPEG = 'mpeg';
    const MPGA = 'mpga';
    const PDF = 'pdf';
    const XLS = 'xls';
    const DEFAULT = 'default';
}
