<?php

use App\Http\Controllers\Admin\Agency\CompanyController;
use App\Http\Controllers\Admin\Agency\EmployeeController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
use \App\Http\Controllers\LanguageController;
use \App\Http\Controllers\Admin\Authentication\AuthController;
use \App\Http\Controllers\Admin\User\UserController;
use \App\Http\Controllers\Admin\User\RoleController;

Route::post('login', [AuthController::class, 'login'])->name('admin.login');
Route::get('login', [AuthController::class, 'getAdminLogin'])->name('admin.get.login');
Route::get('/', [AuthController::class, 'index'])->name('admin.get.index');


Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboardEcommerce'])->name('admin.dashboard-ecommerce');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');



    Route::group(['prefix' => 'users'], function () {

        Route::group(['prefix' => 'dashboard'], function () {


            Route::get('index', [UserController::class, 'index'])->name('user.index');
            Route::get('/pagination/fetch-data', [UserController::class, 'fetchData'])->name('user.fetchData');
            Route::get('create', [UserController::class, 'create'])->name('user.create');
            Route::get('show/{id}', [UserController::class, 'show'])->name('user.show');
            Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
            Route::post('update/{id}', [UserController::class, 'update'])->name('user.update');
            Route::post('store', [UserController::class, 'store'])->name('user.store');
            Route::delete('delete/{id}', [UserController::class, 'destroy'])->name('user.destroy');
            Route::get('profile/{id}', [UserController::class, 'profile'])->name('user.profile');
            Route::get('export', [UserController::class, 'export'])->name('user.export');

        });

        Route::group(['prefix' => 'role'], function () {

            Route::get('index', [RoleController::class, 'index'])->name('role.index');
            Route::get('create', [RoleController::class, 'create'])->name('role.create');
            Route::get('show/{id}', [RoleController::class, 'show'])->name('role.show');
            Route::get('edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
            Route::post('update/{id}', [RoleController::class, 'update'])->name('role.update');
            Route::post('store', [RoleController::class, 'store'])->name('role.store');
            Route::delete('delete/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
            Route::get('export', [RoleController::class, 'export'])->name('role.export');

        });


    });

    Route::group(['prefix' => 'agencies'], function () {

        Route::group(['prefix' => 'companies'], function () {

            Route::get('index', [CompanyController::class, 'index'])->name('company.index');
            Route::get('/pagination/fetch-data', [CompanyController::class, 'fetchData'])->name('company.fetch_data');
            Route::get('create', [CompanyController::class, 'create'])->name('company.create');
            Route::get('show/{id}', [CompanyController::class, 'show'])->name('company.show');
            Route::get('edit/{id}', [CompanyController::class, 'edit'])->name('company.edit');
            Route::post('update/{id}', [CompanyController::class, 'update'])->name('company.update');
            Route::post('store', [CompanyController::class, 'store'])->name('company.store');
            Route::delete('delete/{id}', [CompanyController::class, 'destroy'])->name('company.destroy');
            Route::get('export', [CompanyController::class, 'export'])->name('company.export');

        });


        Route::group(['prefix' => 'employees'], function () {

            Route::get('index', [EmployeeController::class, 'index'])->name('employee.index');
            Route::get('/pagination/fetch-data', [EmployeeController::class, 'fetchData'])->name('employee.fetch_data');
            Route::get('create', [EmployeeController::class, 'create'])->name('employee.create');
            Route::get('show/{id}', [EmployeeController::class, 'show'])->name('employee.show');
            Route::get('edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
            Route::post('update/{id}', [EmployeeController::class, 'update'])->name('employee.update');
            Route::post('store', [EmployeeController::class, 'store'])->name('employee.store');
            Route::delete('delete/{id}', [EmployeeController::class, 'destroy'])->name('employee.destroy');
            Route::get('export', [EmployeeController::class, 'export'])->name('employee.export');

        });


    });


    Route::get('lang/{locale}', [LanguageController::class, 'swap'])->name('lang.swap');

});

