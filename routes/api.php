<?php

use App\Http\Controllers\Api\AuthenticationController;
use App\Http\Controllers\Api\ContentController;
use App\Http\Controllers\Api\CreditController;
use App\Http\Controllers\Api\CurrenciesController;
use App\Http\Controllers\Api\LocationController;
use App\Http\Controllers\Api\MainSettingController;
use App\Http\Controllers\Api\OfferController;
use App\Http\Controllers\Api\SaleBuyController;
use App\Http\Controllers\Api\TransferIncomingController;
use App\Http\Controllers\Api\TransferOutgoingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['MobileLocal'])->group(function () {


    //Authentication
    Route::post('/login-regular', [AuthenticationController::class, 'login'])->name('api.login.regular')->middleware(['throttle:10,5']);
    Route::post('/register-regular', [AuthenticationController::class, 'register'])->name('api.login.register')->middleware(['throttle:10,5']);
    Route::post('/forget-password', [AuthenticationController::class, 'forgetPassword'])->name('api.login.forgetPassword')->middleware(['throttle:10,5']);
    Route::get('/get-agency-value', [AuthenticationController::class, 'getAgencyValue'])->name('api.login.getAgencyValue');


    //Location
    Route::get('/get-countries', [LocationController::class, 'getCountry'])->name('api.getCountry');
    Route::get('/get-city-by-country', [LocationController::class, 'getCityByCountry'])->name('api.getCityByCountry');


    //Content
    Route::get('/get-about-us', [ContentController::class, 'getAboutUs'])->name('api.content.getAboutUs');
    Route::get('/get-privacy-policy', [ContentController::class, 'getPrivacy'])->name('api.content.getPrivacy');
    Route::get('/get-payment-policy', [ContentController::class, 'getPayment'])->name('api.content.getPayment');
    Route::get('/get-term-condition', [ContentController::class, 'getTerm'])->name('api.content.getTerm');




    Route::middleware(['SanctumAuth'])->group(function () {
        Route::get('/user-profile', [AuthenticationController::class, 'userProfile'])->name('api.userProfile');
        Route::post('/update-profile-image', [AuthenticationController::class, 'userProfileImage'])->name('api.userProfileImage');
        Route::post('/update-profile-password', [AuthenticationController::class, 'userProfilePassword'])->name('api.userProfilePassword');
        Route::get('/get-currencies', [CurrenciesController::class, 'getCurrencies'])->name('api.getCurrencies');



        //Sale || Buy
        Route::post('/sale-buy-post', [SaleBuyController::class, 'storeSaleBuy'])->name('api.sale.storeSaleBuy');
        Route::get('/get-sale-buy', [SaleBuyController::class, 'getSaleBuy'])->name('api.sale.getSaleBuy');
        Route::post('/update-sale-buy', [SaleBuyController::class, 'updateSaleBuy'])->name('api.sale.updateSaleBuy');
        Route::post('/get-all-sale-buy', [SaleBuyController::class, 'getAllSaleBuy'])->name('api.sale.getAllSaleBuy');
        Route::get('/get-sale-buy-status', [SaleBuyController::class, 'getSaleBuyStatus'])->name('api.sale.getSaleBuyStatus');
        Route::post('/sale-buy-delete', [SaleBuyController::class, 'SaleBuyDelete'])->name('api.sale.SaleBuyDelete');
        Route::post('/update-sale-buy-status', [SaleBuyController::class, 'updateSaleBuyStatus'])->name('api.sale.updateSaleBuyStatus');



        //Order || Offer
        Route::post('/order-ofer-post', [OfferController::class, 'storeOffer'])->name('api.offer.storeSaleBuy');
        Route::get('/get-order-offer', [OfferController::class, 'getOffer'])->name('api.offer.getOffer');
        Route::post('/update-order-ofer', [OfferController::class, 'updateOrderOffer'])->name('api.offer.updateOrderOffer');
        Route::post('/get-all-order-offer', [OfferController::class, 'getAllOrderOffer'])->name('api.offer.getAllOrderOffer');
        Route::get('/get-offer-order-status', [OfferController::class, 'getOfferOrderStatus'])->name('api.offer.getOfferOrderStatus');
        Route::get('/get-all-type-users', [OfferController::class, 'getAllTypeUsers'])->name('api.offer.getAllTypeUsers');
        Route::post('/complete-information-order-offer', [OfferController::class, 'completeInformation'])->name('api.offer.completeInformation');
        Route::post('/order-offer-delete', [OfferController::class, 'orderOfferDelete'])->name('api.sale.orderOfferDelete');
        Route::get('/get-commission-type', [OfferController::class, 'getCommissionType'])->name('api.offer.getCommissionType');
        Route::post('/update-order-offer-status', [OfferController::class, 'updateOrderOfferStatus'])->name('api.sale.updateOrderOfferStatus');


        //Outgoing && incoming transfer
        Route::post('/get-all-transfer', [TransferOutgoingController::class, 'getAllTransfer'])->name('api.outgoing.getAllTransfer');
        Route::post('/change-transfer-status', [TransferOutgoingController::class, 'changeTransferStatus'])->name('api.outgoing.changeTransferStatus');

        //Outgoing transfer
        Route::post('/store-outgoing-transfer', [TransferOutgoingController::class, 'storeOutgoingTransfer'])->name('api.outgoing.storeIncomingTransfer');
        Route::get('/get-outgoing-transfer', [TransferOutgoingController::class, 'getOutgoingTransfer'])->name('api.outgoing.getOutgoingTransfer');
        Route::post('/update-outgoing-transfer', [TransferOutgoingController::class, 'updateOutgoingTransfer'])->name('api.outgoing.updateOutgoingTransfer');
        Route::post('/delete-outgoing-transfer', [TransferOutgoingController::class, 'deleteOutgoingTransfer'])->name('api.outgoing.deleteOutgoingTransfer');


        //Credit
        Route::post('/get-all-credit', [CreditController::class, 'index'])->name('api.credit.index');
        Route::post('/store-credit', [CreditController::class, 'store'])->name('api.credit.store');
        Route::post('/update-credit', [CreditController::class, 'update'])->name('api.credit.update');
        Route::get('/get-credit-by-id', [CreditController::class, 'show'])->name('api.credit.show');
        Route::post('/delete-credit-by-id', [CreditController::class, 'destroy'])->name('api.credit.destroy');
        Route::post('/change-credit-status', [CreditController::class, 'changeCreditStatus'])->name('api.credit.changeCreditStatus');
        Route::get('/get-credit-status', [CreditController::class, 'getCreditStatus'])->name('api.credit.getCreditStatus');


        //hide item
        Route::post('/hide-item', [MainSettingController::class, 'hideItem'])->name('api.setting.hideItem');


        //Authentication
        Route::post('/logout-regular', [AuthenticationController::class, 'logout'])->name('api.login.logout')->middleware(['throttle:10,5']);


        //agencies
        Route::get('/get-all-branch', [AuthenticationController::class, 'getAllBranches'])->name('api.authentication.getAllBranches');
        Route::get('/get-all-dealers', [AuthenticationController::class, 'getAllDealers'])->name('api.authentication.getAllDealers');
        Route::get('/get-dealers-branches', [AuthenticationController::class, 'getDealersBranches'])->name('api.authentication.getDealersBranches');
        Route::get('/get-all-users', [AuthenticationController::class, 'getAllUsers'])->name('api.authentication.getAllUsers');



        //Search all
        Route::get('/get-all-users-search', [AuthenticationController::class, 'getAllUsersSearch'])->name('api.authentication.getAllUsersSearch');


    });

});
