<?php

return [

    'success' => 'The data created successful',
    'delete' => 'The data deleted successful',
    'failed' => 'There is an error',
    'string' => 'The field must be a string',
    'in' => 'The field must be 0 or 1',
    'update' => 'The data updated successful',
    'accept' => 'The order accepted successful',
    'refuse' => 'The order refused successful',
    'login_active_failed' => 'Login Failed The User not Active',
    'login_username_password_wrong' => 'Login Failed The Username or Password was Wrong',
    'login_verify_failed' => 'User login failed Unconfirmed account',
    "login_success" => 'You have logged in successfully, welcome',
    "complete_success" => 'The information has been completed successfully'

];
