<?php

return [


    "Users" => 'Users',
    "Dashboards" => "Dashboards",
    "Employees" => 'Employees',
    "Roles" => 'Roles',
    "Participants" => 'Participants',
    "System Users" => 'System Users',
    "Agencies" => 'Agencies',
    "Companies" => 'Companies'
];
