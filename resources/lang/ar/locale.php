<?php

return [

    "Users" => 'المستخدمين',
    "Dashboards" => "الواجهة الرئيسية",
    "Employees" => 'الموظفين',
    "Roles" => 'الصلاحيات',
    "Participants" => 'المشتريكن',
    "System Users" => 'مستخدمين النظام',
    "Agencies" => 'عملائنا',
    "Companies" => 'الشركات'

];
