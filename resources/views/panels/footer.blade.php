<!-- BEGIN: Footer-->
<footer class="footer footer-light {{($configData['footerType'] === 'footer-hidden') ? 'd-none':''}} {{$configData['footerType']}}">
  <p style="display: flex;
    align-items: center;
    justify-content: center;" class="clearfix mb-0">
    <span @if(app()->getLocale() == 'en') class="float-md-start d-block d-md-inline-block mt-25" @else class="float-md-end d-block d-md-inline-block mt-25" @endif >COPYRIGHT &copy;
      <script>document.write(new Date().getFullYear())</script><a class="ms-25" href="#" target="_blank">{{__('Mohamad Al Moazen')}}</a>,
      <span class="d-none d-sm-inline-block">{{__('All rights Reserved')}}</span>
    </span>
{{--    <span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span>--}}
  </p>
</footer>
{{--<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>--}}
<!-- END: Footer-->
