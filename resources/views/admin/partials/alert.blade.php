@if(\Illuminate\Support\Facades\Session::has('success'))


    <script>
        swal("{{Session::get('success')}}", "{{__('Click ok to continue')}}", "success");
    </script>

    @php
        Session::forget('success');
    @endphp

@elseif(\Illuminate\Support\Facades\Session::has('failed'))


    <script>
        swal("{{Session::get('failed')}}", "{{__('Something went wrong!')}}", "error");
    </script>

    @php
        Session::forget('failed');
    @endphp

@endif
