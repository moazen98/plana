@extends('layouts/contentLayoutMaster')

@section('title', __('Show Roles'))

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')


    @include('admin.partials.alert')

    <form action="{{route('role.index')}}" method="GET">

        <div class="row">
            <div class="form-group col-4" style="margin-bottom: 10px">
                <input type="text" class="form-control" name="search"
                       placeholder="{{__('Search By Name , Description')}}"
                       value="{{ request()->input('search') }}">
                <span class="text-danger">@error('search'){{ $message }} @enderror</span>
            </div>

            <div class="form-group col-4">
                <button type="submit" class="btn btn-primary">{{__('Search')}}</button>
            </div>

        </div>
    </form>


    <!-- Column Search -->
    <section id="ajax-datatable">
        <div class="row">

            <div class="col-12">


                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">{{__('Roles Info')}}</h4>

                        <div class="row">
                            @permission('create_role')
                            <div class="col-6">
                                <a href="{{route('role.create')}}"
                                   @if(app()->getLocale() == 'en')style="margin-right: 60px!important;white-space: nowrap;max-width: 100%;"
                                   @else style="margin-left: 60px!important;white-space: nowrap;max-width: 100%;" @endif>
                                    <button type="button"
                                            class="btn btn-primary">{{ __('Add Role') }}
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </a>
                            </div>
                            @endpermission

                            <div class="col-6">
                                <a href="{{route('role.export')}}">
                                    <button type="button"
                                            class="btn btn-success">{{ __('Export') }}
                                        <i class="fa fa-file-excel-o"></i>
                                    </button>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="card-datatable overflow-auto">
                        <table class="datatables-ajax table table-responsive">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Description')}}</th>
                                <th>{{__('Employees Number')}}</th>
                                <th>{{ __('Action') }}</th>
                            </tr>

                            <body>
                            @foreach ($roles as $index => $role)
                                <tr>
                                    <td>{{$role['id']}}</td>
                                    <td>{{$role['name']}}</td>
                                    <td>{{$role['description']}}</td>
                                    <td>{{$role['employees_number']}}</td>

                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                                    data-bs-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                @permission('update_role')
                                                <a class="dropdown-item" href="{{route('role.edit',$role['id'])}}">
                                                    <i data-feather="edit-2" class="me-50"></i>
                                                    <span>{{__('Edit')}}</span>
                                                </a>
                                                @endpermission


                                                @permission('read_role')
                                                <a class="dropdown-item" href="{{route('role.show',$role['id'])}}">
                                                    <i data-feather="eye" class="me-50"></i>
                                                    <span>{{__('Show')}}</span>
                                                </a>
                                                @endpermission


                                                @if(!$role['basic_role'])
                                                    @permission('delete_role')
                                                    <div class="col-4">
                                                        <form method="post" id="myForm"
                                                              action="{{route('role.destroy',$role['id'])}}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn delete delete-style dropdown-item"><i
                                                                    data-feather="trash"
                                                                    class="me-50"></i>
                                                                <span>{{__('Delete')}}</span></button>
                                                        </form>
                                                    </div>
                                                    @endpermission
                                                @endif

                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </body>

                            </thead>
                        </table>
                    </div>

                    {{$roles->appends(request()->query())->links("pagination::bootstrap-4")}}
                </div>
            </div>
        </div>
    </section>
    <!--/ Column Search -->



@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    {{--        <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>--}}
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>

@endsection

@include('admin.user.role.include.scripts')

