<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style>

    .pagination {
        justify-content: center !important;
        margin-top: 10px !important;
    }

    td {
        white-space: nowrap;
        max-width: 100%;
    }

</style>


<style>
    select2-container--default {
        display: flex !important;
    }
</style>
<style>
    .form-switch .form-check-label .switch-text-left i, .form-switch .form-check-label .switch-text-left svg, .form-switch .form-check-label .switch-text-right i, .form-switch .form-check-label .switch-text-right svg, .form-switch .form-check-label .switch-icon-left i, .form-switch .form-check-label .switch-icon-left svg, .form-switch .form-check-label .switch-icon-right i, .form-switch .form-check-label .switch-icon-right svg {
        height: 13px;
        width: 13px;
        margin-top: 6px;
        font-size: 13px;
    }
</style>
