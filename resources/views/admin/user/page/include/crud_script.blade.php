

<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                section: "required",
                email: {
                    email : true,
                    required: true
                },
                password : {
                    minlength : 6,
                    required : true,
                },
                confirm_password : {
                    required : true,
                    minlength : 6,
                    equalTo : "#password"
                },
                role: "required",
                country: "required",
                city: "required",
                phone_number: {
                    required : true,
                    number: true
                },
            },
            messages: {
                first_name: {
                    required: "{{trans('validation.required')}}",
                },
                last_name: {
                    required: "{{trans('validation.required')}}",
                },
                section: {
                    required: "{{trans('validation.required')}}",
                },
                email: {
                    required: "{{trans('validation.required')}}",
                    email: "{{trans('validation.email')}}",
                },
                confirm_password: {
                    minlength: "{{trans('validation.min_password')}}",
                    required: "{{trans('validation.required')}}",
                    equalTo: "{{trans('validation.equalTo')}}",
                },
                password: {
                    minlength: "{{trans('validation.min_password')}}",
                    required: "{{trans('validation.required')}}",
                },
                role: {
                    required: "{{trans('validation.required')}}",
                },
                country: {
                    required: "{{trans('validation.required')}}",
                },
                city: {
                    required: "{{trans('validation.required')}}",
                },
                phone_number: {
                    required: "{{trans('validation.required')}}",
                    number: "{{trans('validation.numeric')}}",
                },
            }
        });
    });
</script>



<script>
    $(document).ready(function () {

        $("#regForm2").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                section: "required",
                email: {
                    email : true,
                    required: true
                },
                role: "required",
                country: "required",
                city: "required",
                phone_number: {
                    required : true,
                    number: true
                },
            },
            messages: {
                first_name: {
                    required: "{{trans('validation.required')}}",
                },
                last_name: {
                    required: "{{trans('validation.required')}}",
                },
                section: {
                    required: "{{trans('validation.required')}}",
                },
                email: {
                    required: "{{trans('validation.required')}}",
                    email: "{{trans('validation.email')}}",
                },
                role: {
                    required: "{{trans('validation.required')}}",
                },
                country: {
                    required: "{{trans('validation.required')}}",
                },
                city: {
                    required: "{{trans('validation.required')}}",
                },
                phone_number: {
                    required: "{{trans('validation.required')}}",
                    number: "{{trans('validation.numeric')}}",
                },
            }
        });
    });
</script>
