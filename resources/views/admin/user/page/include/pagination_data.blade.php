@foreach ($users as $index => $user)
    <tr>
        <td>{{$user['id']}}</td>
        <td>
            <div class="avatar"><img style="width:30px;height: 30px" src="{{$user['image_path']}}">
            </div> {{$user['full_name']}}</td>
        <td>{{$user['email']}}</td>

        <td>{{$user['role']}}</td>

        @if(!is_null($user['active']))
            <td>
                    <span
                        class="badge rounded-pill badge-light-{{$user['status_class']}}">{{$user['active_string']}}</span>
            </td>
        @else
            <td>
                    <span
                        class="badge rounded-pill badge-light-danger">{{__('No data')}}</span>
            </td>
        @endif


        @if($user['basic_user'])
            <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{__('Yes')}}</span>
            </td>
        @else
            <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{__('No')}}</span>
            </td>
            @endif


        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_user')
                    <a class="dropdown-item" href="{{route('user.edit',$user['id'])}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_user')
                    <a class="dropdown-item" href="{{route('user.show',$user['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @if(!$user['basic_user'])
                    @permission('delete_user')
                    <form method="post"
                          action="{{route('user.destroy',$user['id'])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash" class="me-50"></i>
                            <span>{{__('Delete')}}</span></button>
                    </form>
                    @endpermission
                    @endif

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $users->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

