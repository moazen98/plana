@extends('layouts.contentLayoutMaster')

@section('title', __('Show Company'))

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <!-- vendor css files part2 -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Inconsolata&family=Roboto+Slab&family=Slabo+27px&family=Sofia&family=Ubuntu+Mono&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">

@endsection

@section('page-style')
    {{-- Page Css files --}}


    <link rel="stylesheet" type="text/css" href="{{asset('css/base/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">

@endsection

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="float-left card-title">{{ __('General Info') }}</h3>
                            <div class="float-right" @if(app()->getLocale() == 'en')style="margin-left: 600px"
                                 @else style="margin-right: 600px" @endif>

                                <div class="row">
                                    @permission('update_company')
                                    <div class="col-5">
                                        <a type="button" class="btn btn-primary"
                                           href="{{ route('company.edit',$company['id']) }}"><i
                                                class="fa fa-edit"></i></a>
                                    </div>
                                    @endpermission

                                    @permission('delete_company')
                                    <div class="col-5">
                                        <form method="post" id="myForm"
                                              action="{{route('company.destroy',$company['id'])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="button" class="btn btn-danger delete" title="{{__('Delete')}}"
                                            ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </div>
                                    @endpermission

                                </div>

                            </div>


                        </div>
                        <div class="user-avatar-section">
                            <div class="d-flex align-items-center flex-column">
                                <img
                                    class="img-fluid rounded mt-3 mb-2"
                                    src="{{$company['files']['url']}}"
                                    height="110"
                                    width="110"
                                    alt="User avatar"
                                />
                                <div class="user-info text-center">
                                    <h4>{{$company['name']}}</h4>

                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="card-body">


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Name') }}:</h4>
                                    <p> {{ $company['name'] }} </p>
                                </div>
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Email') }}:</h4>
                                    <p> {{ $company['email'] }} </p>
                                </div>
                            </div>




                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Website') }}:</h4>
                                    <p> {{ $company['website'] }} </p>
                                </div>

                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Date') }}:</h4>
                                    <p> {{ $company['date'] }} </p>
                                </div>
                            </div>


                            <div class="row mb-2">
                                <div class="col-md-6 col-12">
                                    <h4>{{ __('Employees number') }}:</h4>
                                    <span
                                        class="badge rounded-pill badge-light-success">{{$company['employees_number']}}</span>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>

@endsection
@section('page-script')
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-sweet-alerts.js')) }}"></script>


    @include('admin.partials.scripts')

@endsection
