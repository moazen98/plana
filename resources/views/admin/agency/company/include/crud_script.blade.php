<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                email: {
                    email : true
                },
                website : {
                    url : true,
                },

            },
            messages: {
                email: {
                    email: "{{trans('validation.email')}}",
                },
                website: {
                    url: "{{trans('validation.url')}}",
                },
                "ar[name]": {
                    required: "{{trans('validation.required')}}",
                },
                "en[name]": {
                    required: "{{trans('validation.required')}}",

                },
            }
        });
    });
</script>
