@foreach ($companies as $index => $company)
    <tr>
        <td>{{$company['id']}}</td>
        <td>
            @if(!is_null($company['files']))
                <div class="avatar"><img style="width:30px;height: 30px" src="{{$company['files']['url']}}">
                    @endif
                </div> {{$company['name']}}</td>
        <td>{{$company['email']}}</td>


        <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{$company['employees_number']}}</span>
        </td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_company')
                    <a class="dropdown-item" href="{{route('company.edit',$company['id'])}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_company')
                    <a class="dropdown-item" href="{{route('company.show',$company['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_company')
                    <form method="post"
                          action="{{route('company.destroy',$company['id'])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash"
                                                                                 class="me-50"></i>
                            <span>{{__('Delete')}}</span></button>
                    </form>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $companies->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

