@extends('layouts.contentLayoutMaster')

@section('title', __('Add employee'))

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{asset('vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{asset('css/base/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('build/css/demo.css')}}">

@endsection

@section('content')


    <section id="basic-horizontal-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('Add Employee to the System')}}</h4>
                </div>
                <div class="alert-body">
                    <!-- Vertical Wizard -->
                    <div class="card-body">
                        <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data"
                              id="regForm">
                            @csrf

                            <div class="mb-1">
                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ __('First Name') }}
                                            *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="first_name"
                                            aria-label="Full name"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('first_name')}}" required
                                        />
                                        @error('first_name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ __('Last Name') }} *</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="last_name"
                                            aria-label="last_name"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('last_name')}}" required
                                        />
                                        @error('last_name')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <input type="hidden" id="code" name="international_code" value="90"/>
                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <label class="form-label" for="phone">{{ __('Phone') }}</label>
                                        <br>
                                        <input class="form-control" id="phone" name="phone" type="tel"
                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                               value="{{old('full')}}">
                                        @error('phone')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="form-label" for="basic-addon-name">{{ __('Email') }}</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            name="email"
                                            aria-label="email"
                                            aria-describedby="basic-addon-name"
                                            value="{{old('email')}}"
                                        />
                                        @error('email')
                                        <span class="text-danger">{{$message }}</span>
                                        @enderror
                                    </div>
                                </div>



                                <div class="row col-12 mb-2">
                                    <div class="col-6">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="section">{{__('Employee company')}} *</label>
                                                <select class="select2 form-select" name="company_id" required>
                                                    <option selected disabled>{{__('Please select company')}}</option>
                                                    @foreach($companies['companies'] as $company)
                                                        <option @if(old('company_id') == $company['id']) selected @endif
                                                        value="{{$company['id']}}">{{$company['name']}}</option>
                                                    @endforeach
                                                </select>
                                                @error('company_id')
                                                <span class="text-danger">{{$message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @permission('create_employee')
                            <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                            @endpermission
                        </form>
                    </div>
                    <!-- /Vertical Wizard -->
                </div>
            </div>
        </div>
    </section>

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/extensions/ext-component-clipboard.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/pickers/form-pickers.js')) }}"></script>

    @include('admin.agency.employee.include.crud_script')

    @include('admin.partials.scripts')


@endsection





@endsection


