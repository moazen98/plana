@foreach ($employees as $index => $employee)
    <tr>
        <td>{{$employee['id']}}</td>
        <td>
            {{$employee['full_name']}}</td>
        <td>{{$employee['email']}}</td>
        <td>{{$employee['full_phone']}}</td>

        <td>
                    <span
                        class="badge rounded-pill badge-light-info">{{$employee['company'] == null ? __('No data') : $employee['company']['name']}}</span>
        </td>

        <td>
            <div class="dropdown">
                <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                    <i data-feather="more-vertical"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @permission('update_employee')
                    <a class="dropdown-item" href="{{route('employee.edit',$employee['id'])}}">
                        <i data-feather="edit-2" class="me-50"></i>
                        <span>{{__('Edit')}}</span>
                    </a>
                    @endpermission


                    @permission('read_employee')
                    <a class="dropdown-item" href="{{route('employee.show',$employee['id'])}}">
                        <i data-feather="eye" class="me-50"></i>
                        <span>{{__('Show')}}</span>
                    </a>
                    @endpermission


                    @permission('delete_employee')
                    <form method="post"
                          action="{{route('employee.destroy',$employee['id'])}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn delete delete-style dropdown-item"><i data-feather="trash"
                                                                                 class="me-50"></i>
                            <span>{{__('Delete')}}</span></button>
                    </form>
                    @endpermission

                </div>
            </div>
        </td>


    </tr>
@endforeach
{{--<tr>--}}
<td colspan="3">
    {!! $employees->appends(request()->query())->links("pagination::bootstrap-4") !!}
</td>
{{--</tr>--}}

<script>
    $(document).ready(function () {
        feather.replace()
    })
</script>

