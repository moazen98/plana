<script>
    $(document).ready(function () {

        $("#regForm").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                email: {
                    email : true,
                },
                phone: {
                    number: true
                },
            },
            messages: {
                first_name: {
                    required: "{{trans('validation.required')}}",
                },
                last_name: {
                    required: "{{trans('validation.required')}}",
                },
                email: {
                    email: "{{trans('validation.email')}}",
                },
                phone: {
                    number: "{{trans('validation.numeric')}}",
                },
            }
        });
    });
</script>


