<?php

return [
    'role_structure' => [
        'super_admin' => [
            'user' => 'c,r,u,d',
            'role' => 'c,r,u,d',
            'employee' => 'c,r,u,d',
            'company' => 'c,r,u,d',
        ],
        'admin' => []
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
